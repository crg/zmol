/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_MOLECULE_RENDERER_HPP
#define ZMOL_MOLECULE_RENDERER_HPP

#include <memory>
#include <functional>
#include "opengl3.hpp"
#include "glsl.hpp"
#include "buffer_object.hpp"
#include "vertex_array_object.hpp"
#include "molecule_data.hpp"
#include "noncopyable.hpp"
#include "texture.hpp"
#include "cml/cml.h"


namespace zmol
{


/**
 * Flyweight containing shaders used by molecule renderers.
 *
 * The renderers require several shaders, all of which are compiled in this class. Only one instance has to exist per OpenGL context;
 * multiple molecule renderers can share the same molecule_shaders instance.
 * If the only goal is to use the molecule_renderer, all that needs to be done is to instantiate this class at least once, and pass
 * it to the molecule_renderer constructor; note though, that the molecule_renderer does not take ownership of this class. The easiest
 * way to use this is to just instantiate it, keep it around somewhere (for example, as a class member), and pass it as argument to
 * the molecule_renderers. Destroy the molecule_shaders instance only after all instances of molecule_renderer that use it are destroyed.
 *
 * It is not copyable, to avoid issues with duplicated shaders. (Copying instances of this classes makes no sense anyway.)
 */
class molecule_shaders:
	private noncopyable
{
public:
	// Struct containing the GLSL program as well as GLSL uniform locations into that program.
	struct program_data
	{
		program m_program;
		GLint
			m_model_view_uniform,
			m_projection_uniform,
			m_scale_factor_uniform,
			m_texlen_uniform,
			m_border_and_flatness_uniform,
			m_lighting_params_uniform
			;

		// Constructor just sets the uniform locations to -1 (= "not initialized").
		program_data();
	};


	/**
	 * Constructor. 
	 *
	 * @param p_errormsg_callback Callback to be used for when a GLSL compilation or linking error occurs.
	 */
	explicit molecule_shaders(std::function < void(std::string const &p_errormsg) > const &p_errormsg_callback);


	// The GLSL shaders and programs.
	program_data
		m_sphere_draw_to_screen,
		m_sphere_draw_to_shadowmap,
		m_sphere_draw_to_ao_atlas,
		m_cylinder_draw_to_screen,
		m_cylinder_draw_to_shadowmap,
		m_cylinder_draw_to_ao_atlas
		;
};


/**
 * Class for rendering molecule data.
 *
 * This class takes molecule_data input, creates impostor geometry data out of it, and renders said impostors.
 * In addition, it calculates ambient occlusion, and stores the results in a texture atlas.
 *
 * The renderer uses its own orthogonal projection matrix, and paints into a designated viewport region.
 * It is not possible to use a custom projection matrix.
 * 
 * The class is not stateless, but mostly immutable; no other molecule_data can be passed to it after construction.
 * Certain parameters, such as the atlas texture size, the screen size, the rendering mode, are mutable.
 */
class molecule_renderer:
	private noncopyable
{
public:
	/**
	 * Rendering modes.
	 *
	 * @todo Currently, only the space filling mode is implemented.
	 */
	enum rendering_modes
	{
		rendering_mode_space_filling, /**< Space-filling rendering mode: Atoms are drawn as spheres. Their radius depends on the atomic element. Bonds are not rendered. */
		rendering_mode_balls_and_sticks, /**< Balls-and-sticks rendering mode: Atoms are drawn as spheres. Bonds are drawn as cylinders. All cylinders use the same radius. Compared to space-filling, the spheres are smaller. */
		rendering_mode_licorice /**< Licorice rendering mode: Similar to balls-and-sticks, but the spheres' radius equals the cylinder's. */
	};

	/**
	 * Atom color modes, defining how each atom is colored.
	 */
	enum atom_color_modes
	{
		atom_color_mode_per_element, /**< Color atoms by their individual element */
		atom_color_mode_per_chain /**< Color atoms by the chain they belong to */
	};


	/**
	 * 4x4 matrix type.
	 */
	typedef cml::matrix44f_c matrix;


	/**
	 * Constructor.
	 *
	 * Creates the impostor geometry out of the molecule structure information described by p_data, sets the initial viewport, and calculates
	 * the ambient occlusion, whose results are stored in a texture atlas.
	 * It uses a molecule_data instance as input. The molecule_data structure referred to by p_data is copied internally, so the referred structure
	 * can be destroyed safely after the constructor was executed.
	 *
	 * @note Unlike p_data, the instance referred to by p_molecule_shaders is *not* copied internally, only a reference is kept around. Do not destroy
	 * it before this molecule_renderer is!
	 *
	 * @note The constructor expects a valid OpenGL context to be initialized already, and invokes OpenGL functions. If no OpenGL context exists,
	 * the behavior is undefined (it will most likely crash).
	 *
	 * @param p_molecule_shaders Shaders structure to use for rendering
	 * @param p_data Input molecule data; an internal copy of it is made by the constructor
	 * @param p_initial_left_viewport_offset Initial left viewport offset
	 * @param p_initial_top_viewport_offset Initial top viewport offset
	 * @param p_initial_viewport_width Initial viewport width
	 * @param p_initial_viewport_height Initial viewport height
	 * @param p_initial_atlas_size Initial texture atlas size (= sidelength; the atlas texture is always a square)
	 * @param p_initial_radius_scale Initial molecule scale, default value 1.0f (= 100%)
	 * @param p_initial_rendering_mode Initial rendering mode, default mode is rendering_mode_space_filling
	 * @param p_initial_atom_color_mode Initial color mode, default mode is atom_color_mode_per_element
	 */
	explicit molecule_renderer(
		molecule_shaders &p_molecule_shaders,
		molecule_data const &p_data,
		unsigned int const p_initial_left_viewport_offset, unsigned int const p_initial_top_viewport_offset,
		unsigned int const p_initial_viewport_width, unsigned int const p_initial_viewport_height,
		unsigned int const p_initial_atlas_size,
		float const p_initial_radius_scale = 1.0f,
		rendering_modes const p_initial_rendering_mode = rendering_mode_space_filling,
		atom_color_modes const p_initial_atom_color_mode = atom_color_mode_per_element
	);
	/**
	 * Constructor.
	 *
	 * Creates the impostor geometry out of the molecule structure information described by p_data, sets the initial viewport, and calculates
	 * the ambient occlusion, whose results are stored in a texture atlas.
	 * It uses a molecule_data instance as input. The molecule_data structure referred to by p_data is moved to an internal instance;
	 * after the constructor ran, do not use the referred to by p_data anymore.
	 *
	 * @note Unlike p_data, the instance referred to by p_molecule_shaders is *not* moved to an internal instance, only a reference is kept around.
	 * Do not destroy it before this molecule_renderer is!
	 *
	 * @note The constructor expects a valid OpenGL context to be initialized already, and invokes OpenGL functions. If no OpenGL context exists,
	 * the behavior is undefined (it will most likely crash).
	 *
	 * @param p_molecule_shaders Shaders structure to use for rendering
	 * @param p_data Input molecule data; its contents are moved to an internal molecule_data instance (see C++11 move semantics)
	 * @param p_initial_left_viewport_offset Initial left viewport offset
	 * @param p_initial_top_viewport_offset Initial top viewport offset
	 * @param p_initial_viewport_width Initial viewport width
	 * @param p_initial_viewport_height Initial viewport height
	 * @param p_initial_atlas_size Initial atlas texture size (= sidelength; the atlas texture is always a square)
	 * @param p_initial_radius_scale Initial molecule scale, default value 1.0f (= 100%)
	 * @param p_initial_rendering_mode Initial rendering mode, default mode is rendering_mode_space_filling
	 * @param p_initial_atom_color_mode Initial color mode, default mode is atom_color_mode_per_element
	 */
	explicit molecule_renderer(
		molecule_shaders &p_molecule_shaders,
		molecule_data &&p_data,
		unsigned int const p_initial_left_viewport_offset, unsigned int const p_initial_top_viewport_offset,
		unsigned int const p_initial_viewport_width, unsigned int const p_initial_viewport_height,
		unsigned int const p_initial_atlas_size,
		float const p_initial_radius_scale = 1.0f,
		rendering_modes const p_initial_rendering_mode = rendering_mode_space_filling,
		atom_color_modes const p_initial_atom_color_mode = atom_color_mode_per_element
	);

	/**
	 * Sets the current rendering mode.
	 *
	 * This call reinitializes the impostor geometry appropiately, and also recalculates the ambient occlusion.
	 * If p_new_rendering_mode equals the currently set rendering mode, this call does nothing.
	 *
	 * @param p_new_rendering_mode New rendering mode to use
	 */
	void set_rendering_mode(rendering_modes const p_new_rendering_mode);

	/**
	 * Sets the current atom color mode.
	 *
	 * This call reinitialozes the impostor geometry, but does not recalculate the ambient occlusion (since it would not change).
	 * If p_new_atom_color_mode equals the currently set atom color mode, this call does nothing.
	 *
	 * @param p_new_atom_color_mode New atom color mode to use
	 */
	void set_atom_color_mode(atom_color_modes const p_new_atom_color_mode);

	/**
	 * Sets the current viewport.
	 *
	 * It does not impostor geometry and ambient occlusion data intact, so it is safe to call this often, for example, when a
	 * rendering window gets resized by the user.
	 *
	 * @param p_new_left_viewport_offset New left viewport offset
	 * @param p_new_top_viewport_offset New top viewport offset
	 * @param p_new_viewport_width New viewport width
	 * @param p_new_viewport_height New viewport height
	 */
	void set_viewport(unsigned int const p_new_left_viewport_offset, unsigned int const p_new_top_viewport_offset, unsigned int const p_new_viewport_width, unsigned int const p_new_viewport_height);

	/**
	 * Sets the radiu�s scale.
	 *
	 * @param p_new_radius_scale The new relative scale to use. 1.0f means 100% (= the original radius scale).
	 */
	void set_radius_scale(float const p_new_radius_scale);

	/**
	 * Sets the border width.
	 *
	 * @param p_new_border_width The new border width to use. Minimum valid value is 0.0f (= no border).
	 */
	void set_border_width(float const p_new_border_width);

	/**
	 * Sets the border variance.
	 *
	 * @param p_new_border_variance The new border variance to use. Minimum valid value is 0.0f.
	 */
	void set_border_variance(float const p_new_border_variance);

	/**
	 * Sets the ambient occlusion strength.
	 *
	 * @param p_new_ao_strength The new ambient occlusion strength to use.
	 */
	void set_ao_strength(float const p_new_ao_strength);

	/**
	 * Sets the direct lighting strength.
	 *
	 * @param p_new_direct_lighting_strength The new direct lighting strength to use.
	 */
	void set_direct_lighting_strength(float const p_new_direct_lighting_strength);

	/**
	 * Sets the shininess.
	 *
	 * @param p_new_shininess The new shininess to use.
	 */
	void set_shininess(float const p_new_shininess);

	/**
	 * Sets the glossiness.
	 *
	 * @param p_new_glossiness The new glossiness to use.
	 */
	void set_glossiness(float const p_new_glossiness);

	/**
	 * Sets the flatness.
	 *
	 * @param p_new_flatness The new flatness to use.
	 */
	void set_flatness(float const p_new_flatness);

	/**
	 * Sets the atlas texture size.
	 *
	 * @param p_new_atlas_size The new atlas texture size (= sidelength; the atlas texture is always a square)
	 */
	void set_atlas_size(unsigned int const p_new_atlas_size);

	/**
	 * Renders one frame into the current viewport.
	 *
	 * @note This call expects glViewport() to have been called, which is done by set_viewport.
	 * If glViewport is called by some other code, make sure the viewport is set back to the original values,
	 * otherwise the rendering might not be correct.
	 *
	 * @param p_rotation Rotation quaternion to use as the molecule rendering's rotation.
	 */
	void render(cml::quaternionf const &p_rotation);

	/**
	 * Retrieves the current projection matrix.
	 *
	 * @return Current projection matrix
	 */
	matrix const & get_projection() const { return m_projection; }


protected:
	typedef std::unique_ptr < texture > texture_uptr;


	// Rendering always happens in two passes: first the spheres, then the cylinders.
	enum passes
	{
		pass_spheres,
		pass_cylinders
	};


	// Common initializer for all the constructors.
	void init_renderer();

	// Internal rendering function for a specific pass using a given GLSL program.
	void render_internal(molecule_shaders::program_data &p_program_data, matrix const &p_rotation, matrix const &p_projection, passes const p_pass);

	// Calculates ambient occlusion; expects the impostor geometry to exist already, so calc_quads() must be called prior to this.
	void calc_ao();

	// Calculates impostor quads. This always needs to be called before the ambient occlusion term is calculated (= before calc_ao() is called).
	void calc_quads();

	// Initializes the atlas texture instance, that is, the value of m_ao_atlas.
	void init_atlas_texture();


	molecule_shaders &m_shaders;
	matrix m_projection;
	vertex_array_object m_vao;
	buffer_object m_vertices, m_indices;
	molecule_data m_data;
	std::size_t m_num_atoms, m_num_bonds;
	float m_scale_factor; // Factor to scale atom and cylinder radii and atom positions; depends on the overall size of the molecule
	float m_radius_scale; // User-defined scale by which the sphere and cylinder radii are scaled
	float m_border_width, m_border_variance;
	float m_ao_strength, m_direct_lighting_strength, m_shininess, m_glossiness, m_flatness;
	cml::vector3f m_base_translation;
	unsigned int m_left_viewport_offset, m_top_viewport_offset, m_viewport_width, m_viewport_height;
	unsigned int m_atlas_size;
	rendering_modes m_rendering_mode;
	atom_color_modes m_atom_color_mode;

	texture_uptr m_ao_atlas;
	std::size_t m_ao_patch_sidelength;
};


}


#endif

