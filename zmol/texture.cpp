/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include "texture.hpp"


namespace zmol
{


texture::texture(GLsizei const p_width, GLsizei const p_height, GLint const p_internal_format, GLenum const p_format, GLenum const p_type):
	m_width(p_width),
	m_height(p_height),
	m_internal_format(p_internal_format)
{
	glGenTextures(1 , &m_name);
	bind(0);
	glTexImage2D(target, 0, p_internal_format, p_width, p_height, 0, p_format, p_type, 0);
	glTexParameteri(target, GL_TEXTURE_MAX_LEVEL, 0);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	unbind(0);
}


texture::~texture()
{
	if (m_name != 0)
		glDeleteTextures(1 , &m_name);
}


void texture::bind(GLuint const p_unit)
{
	glActiveTexture(GL_TEXTURE0 + p_unit);
	glBindTexture(target, m_name);
}


void texture::unbind(GLuint const p_unit)
{
	glActiveTexture(GL_TEXTURE0 + p_unit);
	glBindTexture(target, 0);
}


namespace
{


bool is_compressed_texture_format(GLint const p_format)
{
	switch (p_format)
	{
		case GL_COMPRESSED_RGB:
		case GL_COMPRESSED_RGBA:
		case GL_COMPRESSED_SRGB:
		case GL_COMPRESSED_SRGB_ALPHA:
		case GL_COMPRESSED_RED:
		case GL_COMPRESSED_RG:
		case GL_COMPRESSED_RED_RGTC1:
		case GL_COMPRESSED_SIGNED_RED_RGTC1:
		case GL_COMPRESSED_RG_RGTC2:
		case GL_COMPRESSED_SIGNED_RG_RGTC2:
		case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
		case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
		case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
		case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
			return true;

		default:
			return false;
	}

	return false;
}


}


void texture::upload_pixels(void const *p_pixels, unsigned int const p_num_pixel_bytes, GLenum const p_pixel_data_format, GLenum const p_pixel_data_type)
{
	if (is_compressed_texture_format(m_internal_format))
		glCompressedTexSubImage2D(target, 0, 0, 0, m_width, m_height, GLenum(m_internal_format), p_num_pixel_bytes, p_pixels);
	else
		glTexSubImage2D(target, 0, 0, 0, m_width, m_height, p_pixel_data_format, p_pixel_data_type, p_pixels);
}


}

