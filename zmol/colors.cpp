/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include <utility>
#include "colors.hpp"


namespace zmol
{


namespace
{


struct color_entry
{
	char const * m_name;
	int m_red, m_green, m_blue;
};


color_entry const element_color_entries[] =
{
	{" H  ", 255,255,255 },
	{"HE  ", 217,255,255 },
	{"LI  ", 204,128,255 },
	{"BE  ", 194,255,  0 },
	{" B  ", 255,181,181 },
	{" C  ", 144,144,144 },
	{" N  ",  48, 80,248 },
	{" O  ", 255, 13, 13 },
	{" F  ", 144,224, 80 },
	{"NE  ", 179,227,245 },
	{"NA  ", 171, 92,242 },
	{"MG  ", 138,255,  0 },
	{"AL  ", 191,166,166 },
	{"SI  ", 240,200,160 },
	{" P  ", 255,128,  0 },
	{" S  ", 255,255, 48 },
	{"CL  ",  31,240, 31 },
	{"AR  ", 128,209,227 },
	{"K  ", 143, 64,212 },
	{"CA  ",  61,255,  0 },
	{"SC  ", 230,230,230 },
	{"TI  ", 191,194,199 },
	{" V  ", 166,166,171 },
	{"CR  ", 138,153,199 },
	{"MN  ", 156,122,199 },
	{"FE  ", 224,102, 51 },
	{"CO  ", 240,144,160 },
	{"NI  ",  80,208, 80 },
	{"CU  ", 200,128, 51 },
	{"ZN  ", 125,128,176 },
	{"GA  ", 194,143,143 },
	{"GE  ", 102,143,143 },
	{"AS  ", 189,128,227 },
	{"SE  ", 255,161,  0 },
	{"BR  ", 166, 41, 41 },
	{"KR  ",  92,184,209 },
	{"RB  ", 112, 46,176 },
	{"SR  ",   0,255,  0 },
	{" Y  ", 148,255,255 },
	{"ZR  ", 148,224,224 },
	{"NB  ", 115,194,201 },
	{"MO  ",  84,181,181 },
	{"TC  ",  59,158,158 },
	{"RU  ",  36,143,143 },
	{"RH  ",  10,125,140 },
	{"PD  ",   0,105,133 },
	{"AG  ", 192,192,192 },
	{"CD  ", 255,217,143 },
	{"IN  ", 166,117,115 },
	{"SN  ", 102,128,128 },
	{"SB  ", 158, 99,181 },
	{"TE  ", 212,122,  0 },
	{" I  ", 148,  0,148 },
	{"XE  ",  66,158,176 },
	{"CS  ",  87, 23,143 },
	{"BA  ",   0,201,  0 },
	{"LA  ", 112,212,255 },
	{"CE  ", 255,255,199 },
	{"PR  ", 217,255,199 },
	{"ND  ", 199,255,199 },
	{"PM  ", 163,255,199 },
	{"SM  ", 143,255,199 },
	{"EU  ",  97,255,199 },
	{"GD  ",  69,255,199 },
	{"TB  ",  48,255,199 },
	{"DY  ",  31,255,199 },
	{"HO  ",   0,255,156 },
	{"ER  ",   0,230,117 },
	{"TM  ",   0,212, 82 },
	{"YB  ",   0,191, 56 },
	{"LU  ",   0,171, 36 },
	{"HF  ",  77,194,255 },
	{"TA  ",  77,166,255 },
	{" W  ",  33,148,214 },
	{"RE  ",  38,125,171 },
	{"OS  ",  38,102,150 },
	{"IR  ",  23, 84,135 },
	{"PT  ", 208,208,224 },
	{"AU  ", 255,209, 35 },
	{"HG  ", 184,184,208 },
	{"TL  ", 166, 84, 77 },
	{"PB  ",  87, 89, 97 },
	{"BI  ", 158, 79,181 },
	{"PO  ", 171, 92,  0 },
	{"AT  ", 117, 79, 69 },
	{"RN  ",  66,130,150 },
	{"FR  ",  66,  0,102 },
	{"RA  ",   0,125,  0 },
	{"AC  ", 112,171,250 },
	{"TH  ",   0,186,255 },
	{"PA  ",   0,161,255 },
	{" U  ",   0,143,255 },
	{"NP  ",   0,128,255 },
	{"PU  ",   0,107,255 },
	{"AM  ",  84, 92,242 },
	{"CM  ", 120, 92,227 },
	{"BK  ", 138, 79,227 },
	{"CF  ", 161, 54,212 },
	{"ES  ", 179, 31,212 },
	{"FM  ", 179, 31,186 },
	{"MD  ", 179, 13,166 },
	{"NO  ", 189, 13,135 },
	{"LR  ", 199,  0,102 },
	{"RF  ", 204,  0, 89 },
	{"DB  ", 209,  0, 79 },
	{"SG  ", 217,  0, 69 },
	{"BH  ", 224,  0, 56 },
	{"HS  ", 230,  0, 46 },
	{"MT  ", 235,  0, 38 },

	{ 0, 0, 0, 0 }
};


color_entry const chain_color_entries[] =
{
	{ "A", 192, 208, 255 },
	{ "B", 176, 255, 176 },
	{ "C", 255, 192, 200 },
	{ "D", 255, 255, 128 },
	{ "E", 255, 192, 255 },
	{ "F", 176, 240, 240 },
	{ "G", 255, 208, 112 },
	{ "H", 240, 128, 128 },
	{ "I", 245, 222, 179 },
	{ "J", 0, 191, 255 },
	{ "K", 205, 92, 92 },
	{ "L", 102, 205, 170 },
	{ "M", 154, 205, 50 },
	{ "N", 238, 130, 238 },
	{ "O", 0, 206, 209 },
	{ "P", 0, 255, 127 },
	{ "Q", 60, 179, 113 },
	{ "R", 0, 0, 139 },
	{ "S", 189, 183, 107 },
	{ "T", 0, 100, 0 },
	{ "U", 128, 0, 0 },
	{ "V", 128, 128, 0 },
	{ "W", 128, 0, 128 },
	{ "X", 0, 128, 128 },
	{ "Y", 184, 134, 11 },
	{ "Z", 178, 34, 34 },
	{ "a", 192, 208, 255 },
	{ "b", 176, 255, 176 },
	{ "c", 255, 192, 200 },
	{ "d", 255, 255, 128 },
	{ "e", 255, 192, 255 },
	{ "f", 176, 240, 240 },
	{ "g", 255, 208, 112 },
	{ "h", 240, 128, 128 },
	{ "i", 245, 222, 179 },
	{ "j", 0, 191, 255 },
	{ "k", 205, 92, 92 },
	{ "l", 102, 205, 170 },
	{ "m", 154, 205, 50 },
	{ "n", 238, 130, 238 },
	{ "o", 0, 206, 209 },
	{ "p", 0, 255, 127 },
	{ "q", 60, 179, 113 },
	{ "r", 0, 0, 139 },
	{ "s", 189, 183, 107 },
	{ "t", 0, 100, 0 },
	{ "u", 128, 0, 0 },
	{ "v", 128, 128, 0 },
	{ "w", 128, 0, 128 },
	{ "x", 0, 128, 128 },
	{ "y", 184, 134, 11 },
	{ "z", 178, 34, 34 },
	{ "0", 0, 255, 127 },
	{ "1", 60, 179, 113 },
	{ "2", 0, 0, 139 },
	{ "3", 189, 183, 107 },
	{ "4", 0, 100, 0 },
	{ "5", 128, 0, 0 },
	{ "6", 128, 128, 0 },
	{ "7", 128, 0, 128 },
	{ "8", 0, 128, 128 },
	{ "9", 184, 134, 11 },

	{ 0, 0, 0, 0 }
};


}


colors::colors()
{
	for (color_entry const *entry = element_color_entries; entry->m_name != 0; ++entry)
	{
		m_element_colors[entry->m_name] = cml::vector3f(
			float(entry->m_red) / 255.0f,
			float(entry->m_green) / 255.0f,
			float(entry->m_blue) / 255.0f
		);
	}

	for (color_entry const *entry = chain_color_entries; entry->m_name != 0; ++entry)
	{
		m_chain_colors[entry->m_name] = cml::vector3f(
			float(entry->m_red) / 255.0f,
			float(entry->m_green) / 255.0f,
			float(entry->m_blue) / 255.0f
		);
	}

}


cml::vector3f const & colors::get_color(std::string const &p_name) const
{
	static cml::vector3f const white(1,1,1);

	auto color_iter = m_element_colors.find(p_name);
	if (color_iter == m_element_colors.end())
		return white;
	else
		return color_iter->second;
}


cml::vector3f colors::get_chain_color(std::string const &p_chain_id) const
{
	static cml::vector3f const white(1,1,1);

	auto color_iter = m_chain_colors.find(p_chain_id);
	if (color_iter == m_chain_colors.end())
		return white;
	else
		return color_iter->second;
}


}

