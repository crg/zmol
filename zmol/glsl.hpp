/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_GLSL_HPP
#define ZMOL_GLSL_HPP

#include <string>
#include <set>
#include <unordered_map>
#include <memory>
#include <functional>
#include "opengl3.hpp"
#include "noncopyable.hpp"


namespace zmol
{


/**
 * Shader object wrapper class.
 *
 * The class creates a shader object in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 *
 * Ownership of shader instances can be transferred to program instances. This is useful to make sure
 * that shaders are cleaned up when no programs that need them are around. See program and
 * build_program for details.
 *
 * @note Always construct a shader instance on the heap, and wrap it in a shader::sharedptr instance.
 */
class shader:
	private noncopyable
{
public:
	typedef std::shared_ptr < shader > sharedptr;

	/**
	 * Constructor. Creates a shader object using glCreateShader().
	 *
	 * @note Always construct a shader instance on the heap, and wrap it in a sharedptr instance.
	 *
	 * @param p_type Type of the shader (GL_VERTEX_SHADER/GL_FRAGMENT_SHADER/...).
	 */
	explicit shader(GLenum const p_type);
	/**
	 * Destructor. Destroys a shader object using glDeleteShader().
	 */
	~shader();

	/**
	 * Returns the OpenGL name of the shader object.
	 */
	inline GLuint get_name() const
	{
		return m_name;
	}

	/**
	 * Compiles shader code and stores it in the shader object.
	 * If any compilation errors happen, get_info_log() will return
	 * details.
	 *
	 * @note Depending on the OpenGL drivers, some errors may not be caught
	 * by the compiler, but by the linker. In other words, even if compilation
	 * succeeds, check if linking succeeds as well.
	 *
	 * @param p_code GLSL source code to compile and store.
	 * @return true if compilation was successful, false otherwise.
	 */
	bool compile_and_store(std::string const &p_code);

	/**
	 * Returns information about compilation errors.
	 *
	 * @note Even though this can be called anytime, it is only really useful
	 * to call it when compile_and_store() returned false. Otherwise, the log
	 * may contain confusing or nonsensical information.
	 *
	 * @return Information about what went wrong in the compilation step.
	 */
	std::string get_info_log() const;

protected:
	GLuint m_name;
};




/**
 * Program object wrapper class.
 *
 * The class creates a program object in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 *
 * Ownership of shader instances can be transferred to program instances. This is useful to make sure
 * that shaders are cleaned up when no programs that need them are around. The attach_shader() call 
 * adds a shader's sharedptr to an internal list, the detach_shader() call removes a shader's sharedptr
 * from that list.
 */
class program:
	private noncopyable
{
public:
	/**
	 * Constructor. Creates a program object using glCreateProgram().
	 */
	program();
	/**
	 * Destructor. Destroys a program object using glDeleteProgram().
	 */
	~program();

	/**
	 * Returns the OpenGL name of the program object.
	 */
	inline GLuint get_name() const
	{
		return m_name;
	}

	/**
	 * Attaches a shader to this program object.
	 * Since the shader may be shared by several programs, a shared_ptr is used to keep
	 * track of the shader ownership. (This is also why shader instances must always be allocated
	 * on the heap, and not on the stack.)
	 *
	 * @param p_shader Shader to attach to this program
	 */
	void attach_shader(shader::sharedptr const &p_shader);

	/**
	 * Detaches a shader to this program object.
	 * Since the shader may be shared by several programs, a shared_ptr is used to keep
	 * track of the shader ownership. (This is also why shader instances must always be allocated
	 * on the heap, and not on the stack.)
	 *
	 * @param p_shader Shader to detach from this program
	 */
	void detach_shader(shader::sharedptr const &p_shader);

	/**
	 * Link the program, using the attached shaders.
	 * If something goes wrong during linking, false is returned, and get_info_log() can be used
	 * to get details.
	 *
	 * @return true if linking succeeded, false otherwise.
	 */
	bool link();

	/**
	 * Binds the program object to the OpenGL context, using glUseProgram().
	 */
	void bind();
	/**
	 * Unbinds the program object from the OpenGL context, using glUseProgram().
	 */
	void unbind();

	/**
	 * Returns the index of the attribute with the given name.
	 * @param p_attribute_name Name of the attribute.
	 * @return Index of the attribute, or -1 if no attribute with the given name was found.
	 */
	GLint get_attribute_location(std::string const &p_attribute_name) const;
	/**
	 * Returns the location of the uniform with the given name.
	 * @param p_uniform_name Name of the uniform.
	 * @return Location of the attribute, or -1 if no uniform with the given name was found.
	 */
	GLint get_uniform_location(std::string const &p_uniform_name) const;
	/**
	 * Returns the index of the active uniform block with the given name.
	 * @param p_block_name Name of the active uniform block.
	 * @return Index of the active uniform block, or GL_INVALID_INDEX if no block with the given name was found.
	 */
	GLuint get_uniform_block_index(std::string const &p_block_name) const;

	/**
	 * Returns information about linking errors.
	 *
	 * @note Even though this can be called anytime, it is only really useful
	 * to call it when link() returned false. Otherwise, the log
	 * may contain confusing or nonsensical information.
	 *
	 * @return Information about what went wrong in the linking step.
	 */
	std::string get_info_log() const;

protected:
	typedef std::unordered_map < std::string, GLint > locations_type;
	typedef std::unordered_map < std::string, GLuint > indices_type;
	typedef std::set < shader::sharedptr > glsl_shader_set_type;

	GLuint m_name;
	glsl_shader_set_type m_attached_shaders;
	mutable locations_type m_attribute_locations, m_uniform_locations;
	mutable indices_type m_uniform_block_indices;
};


// glUniformBlockBinding
void bind_uniform_block_to_location(program &p_prog, GLuint const p_block_index, GLuint const p_location);





namespace detail
{


typedef std::function < void(std::string const &p_errormsg) > errormsg_callback;


class build_program_impl
{
public:
	explicit build_program_impl(program &p_prog, errormsg_callback const &p_errormsg_callback);
	build_program_impl& operator()(GLenum const p_type, std::string const &p_identifier, std::string const &p_code);
	build_program_impl& operator()(shader::sharedptr const &p_shaderptr);
	void link();
private:
	program &m_prog;
	errormsg_callback m_errormsg_callback;
};


}


/**
 * Utility function to create a program, compile and attach shaders in one step. It also allows for
 * in-lined GLSL source code.
 *
 * Usage example:
 * @code{.cpp}
 *   shader::sharedptr vs = get_vertex_shader(); // get a vertex shader from somewhere
 *   char const *fshader_source = // < GLSL fragment shader source code
 *   program prog;
 *   build_program(prog, [](std::string const &err) { std::cout << "Error: " << err; })
 *     (vs)
 *     (GL_FRAGMENT_SHADER, "fragment_shader", fshader_source)
 *     ;
 *   prog.link();
 * @endcode
 *
 * This way, the code does not have to perform cumbersome bookkeeping; shaders that are used in only one
 * program can be declared in situ.
 */
detail::build_program_impl build_program(program &p_prog, detail::errormsg_callback const &p_errormsg_callback = detail::errormsg_callback());



}


#endif

