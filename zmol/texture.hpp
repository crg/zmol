/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_TEXTURE_HPP
#define ZMOL_TEXTURE_HPP

#include "opengl3.hpp"
#include "noncopyable.hpp"


namespace zmol
{


/**
 * Texture object wrapper class.
 *
 * The class creates a texture object in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 *
 * The class limits itself to 2D textures, since no other types are necessary for zMol. Also,
 * only one mipmap level is used, together with bilinear filtering. GL_CLAMP_TO_EDGE is used as
 * wrapping mode in the S and T directions.
 */
class texture:
	private noncopyable
{
public:
	enum { target = GL_TEXTURE_2D };

	/**
	 * Constructor. Creates a texture object using glGenTextures().
	 * Memory is reserved for the texture object by using glTexImage2D() with the data pointer
	 * set to null.
	 *
	 * See the OpenGL glTexImage2D() documentation for details about the internal format, the pixel data format, and the pixel data type.
	 *
	 * @param p_width Width of the texture, in pixels
	 * @param p_height Height of the texture, in pixels
	 * @param p_internal_format Internal format of the texture, see 
	 * @param p_format Initial pixel data format (this is necessary, even though only space is reserved for the texture)
	 * @param p_type Initial pixel data type (this is necessary, even though only space is reserved for the texture)
	 */
	explicit texture(GLsizei const p_width, GLsizei const p_height, GLint const p_internal_format, GLenum const p_format, GLenum const p_type);

	/**
	 * Destructor. Destroys the texture object using glDeleteTextures().
	 */
	~texture();

	/**
	 * Returns the width of the texture object, in pixels.
	 */
	inline GLsizei get_width() const { return m_width; }
	/**
	 * Returns the height of the texture object, in pixels.
	 */
	inline GLsizei get_height() const { return m_height; }
	/**
	 * Returns the OpenGL name of the texture object.
	 */
	inline GLuint get_name() const { return m_name; }

	/**
	 * Binds the texture object to the specified texture unit of the OpenGL context.
	 * @param p_unit Texture unit index to bind to
	 */
	void bind(GLuint const p_unit);
	/**
	 * Unbinds the texture object from the specified texture unit of the OpenGL context.
	 * @param p_unit Texture unit index to unbind from
	 */
	static void unbind(GLuint const p_unit);

	/**
	 * Uploads pixel data into the texture object by using glTexSubImage2D().
	 *
	 * See the OpenGL glTexSubImage2D() documentation for details about the the pixel data format and the pixel data type.
	 *
	 * @param p_pixels Pointer to the data block that shall be uploaded
	 * @param p_num_pixel_bytes Size of the data block, in bytes
	 * @param p_pixel_data_format Pixel data format of the data to be uploaded
	 * @param p_pixel_data_type Pixel data type of the data to be uploaded
	 */
	void upload_pixels(void const *p_pixels, unsigned int const p_num_pixel_bytes, GLenum const p_pixel_data_format, GLenum const p_pixel_data_type);

protected:
	GLsizei m_width, m_height;
	GLint m_internal_format;
	GLuint m_name;
};


}


#endif

