/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_COLORS_HPP
#define ZMOL_COLORS_HPP

#include <map>
#include "cml/cml.h"


namespace zmol
{


/**
 * Utility class to get a color for a given element name or chain ID.
 */
class colors
{
public:
	/**
	 * Constructor. Initializes internal color tables.
	 */
	colors();

	/**
	 * Returns an RGB triplet for the given element name.
	 * Channel values range from 0.0 to 1.0.
	 *
	 * @param p_name Element name
	 * @return Associated color, or white (1.0,1.0,1.0) if no such color was found.
	 */
	cml::vector3f const & get_color(std::string const &p_name) const;

	/**
	 * Returns an RGB triplet for the given chain ID.
	 * Channel values range from 0.0 to 1.0.
	 *
	 * @param p_chain_id Chain ID
	 * @return Associated color, or white (1.0,1.0,1.0) if no such color was found.
	 */
	cml::vector3f get_chain_color(std::string const &p_chain_id) const;


private:
	typedef std::map < std::string, cml::vector3f > colors_type;
	colors_type m_element_colors, m_chain_colors;
};


}


#endif

