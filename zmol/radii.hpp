/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_RADII_HPP
#define ZMOL_RADII_HPP

#include <map>
#include "cml/cml.h"


namespace zmol
{


/**
 * Utility class to get a radius for a given element name.
 */
class radii
{
public:
	/**
	 * Constructor. Initializes internal radius tables.
	 */
	radii();

	/**
	 * Returns a radius for the given element name.
	 *
	 * @param p_name Element name
	 * @return Associated radius, or 1.0 if no such radius was found.
	 */
	float get_radius(std::string const &p_name) const;
	/**
	 * Returns a covalent radius for the given element name.
	 *
	 * @param p_name Element name
	 * @return Associated covalent radius, or 1.0 if no such radius was found.
	 */
	float get_covalent_radius(std::string const &p_name) const;


private:
	typedef std::map < std::string, cml::vector2f > radii_type;
	radii_type m_radii;
};


}


#endif

