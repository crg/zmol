/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_OPENGL3_HPP
#define ZMOL_OPENGL3_HPP


#include "GL3/gl3w.h"


#ifndef GL_EXT_texture_compression_s3tc
#define GL_EXT_texture_compression_s3tc 1

#define GL_COMPRESSED_RGB_S3TC_DXT1_EXT 0x83F0
#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT 0x83F1
#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT 0x83F2
#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT 0x83F3

#define GL_COMPRESSED_SRGB_S3TC_DXT1_EXT 0x8C4C
#define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT 0x8C4D
#define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT 0x8C4E
#define GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT 0x8C4F

#endif /* GL_EXT_texture_compression_s3tc */


// TODO: wrap gl3wInit() in another init call which parses the extension strings
// and for example set a GL_EXT_texture_compression_s3tc flag which indicates whether or not this extension
// is supported (missing S3TC support is rare though, mostly Mesa3D, however it is very
// difficult to detect that without knowing the supported extensions)


#endif

