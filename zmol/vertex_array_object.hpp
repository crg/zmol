/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_VERTEX_ARRAY_OBJECT_HPP
#define ZMOL_VERTEX_ARRAY_OBJECT_HPP

#include "noncopyable.hpp"
#include "opengl3.hpp"


namespace zmol
{


/**
 * Vertex array object wrapper class.
 *
 * The class creates a vertex array object in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 */
class vertex_array_object:
	private noncopyable
{
public:
	/**
	 * Constructor. Creates the vertex array object with glGenVertexArrays().
	 */
	vertex_array_object();
	/**
	 * Destructor. Destroys the vertex array object with glDeleteVertexArrays().
	 */
	~vertex_array_object();

	/**
	 * Returns the OpenGL name of the vertex array object.
	 */
	inline GLuint get_name() const
	{
		return m_name;
	}

	/**
	 * Binds the vertex array object to the OpenGL context.
	 */
	void bind();

	/**
	 * Unbinds the vertex array object from the OpenGL context.
	 */
	void unbind();


protected:
	GLuint m_name;
};


}


#endif

