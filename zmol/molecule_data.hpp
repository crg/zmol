/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_MOLECULE_DATA_HPP
#define ZMOL_MOLECULE_DATA_HPP

#include <map>
#include <vector>
#include "cml/cml.h"


namespace zmol
{


/**
 * Data of a molecule, consisting of collections of atoms and bonds.
 *
 * Atoms are described by a name (which is the uppercase version of the element symbol, like "FE", "HG", "HE"), 
 * the ID of the chain it belongs to, and a three-dimensional position. Each atom also has a serial number, which is
 * integer. The indices can be any number, as long as they are unique for each atom in the dataset. They do
 * not have to be consecutive. This is necessary, since some molecule file formats like PDB can have "gaps" in the
 * atom indices; for example, the nth atom has serial nr. 4019, and the (n+1)th one has serial nr. 4025, the
 * (n+2)th one has serial nr. 4026.. also, sometimes the first serial number is 1, sometimes it is 0 etc.
 * Bonds describe a pair of bonded atoms, which are referred to by their serial numbers.
 * Topologically, the overall dataset resembles an undirected graph.
 */
struct molecule_data
{
	/**
	 * Atom structure.
	 */
	struct atom
	{
		std::string m_name; /**< Name of the atom (= the uppercase version of the element symbol, like "FE", "HG", "HE"). */
		std::string m_chain_id; /**< ID of the chain the atom belongs to */
		cml::vector3f m_position; /**< Three-dimensional position of the atom. */
	};


	/**
	 * Bond structure.
	 */
	struct bond
	{
		std::size_t m_serial_numbers[2]; /**< Pair of serial numbers of bonded atoms. */
	};


	typedef std::map < std::size_t, atom > atoms_type;
	atoms_type m_atoms; /**< Associatve collection of atoms. The key is the atom's serial number, the value is the atom structure. */

	typedef std::vector < bond > bonds_type;
	bonds_type m_bonds; /**< Array of bonds. */


	/**
	 * Constructor. Creates an empty dataset.
	 */
	molecule_data();


	/**
	 * Copy constructor.
	 */
	molecule_data(molecule_data const & p_other);


	/**
	 * Move constructor.
	 */
	molecule_data(molecule_data && p_other);


	/**
	 * Move assignment operator.
	 */
	molecule_data& operator = (molecule_data && p_other);
};


}


#endif

