/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_BUFFER_OBJECT_HPP
#define ZMOL_BUFFER_OBJECT_HPP

#include "opengl3.hpp"
#include "noncopyable.hpp"


namespace zmol
{


/**
 * Buffer object wrapper class.
 *
 * The class creates a buffer object in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 *
 * The OpenGL terminology is somewhat counter-intuitive. Informally, these entities are known
 * as "vertex buffer objects", but are used for vertices and indices. The OpenGL target for the former
 * is GL_ARRAY_BUFFER, the latter is GL_ELEMENT_ARRAY_BUFFER. For sake of clarity, buffers for vertices
 * are simply called "vertex buffer objects", and buffers for indices "index buffer objects".
 */
class buffer_object:
	private noncopyable
{
public:
	/**
	 * Constructor. Creates the buffer object with glGenBuffers().
	 *
	 * @param p_target See the OpenGL glBindBuffer() documentation for an explanation of this value
	 * @param p_usage See the OpenGL glBindBuffer() documentation for an explanation of this value
	 */
	explicit buffer_object(GLenum const p_target, GLenum const p_usage);
	/**
	 * Destructor. Destroys the buffer object with glDeleteBuffers().
	 */
	~buffer_object();

	/**
	 * Returns the OpenGL name of the buffer object.
	 */
	inline GLuint get_name() const
	{
		return m_name;
	}

	/**
	 * Returns the target of the buffer object.
	 */
	inline GLenum get_target() const
	{
		return m_target;
	}

	/**
	 * Returns the usage of the buffer object.
	 */
	inline GLenum get_usage() const
	{
		return m_usage;
	}

	/**
	 * Binds the buffer object to the OpenGL context.
	 */
	void bind();
	/**
	 * Unbinds the buffer object to the OpenGL context.
	 */
	void unbind();

	/**
	 * Uploads a block of data into the buffer object.
	 * Previous contents will be erased by this call.
	 * 
	 * @param p_data Pointer to the data block to be uploaded
	 * @param p_num_bytes Size of the data block to be uploaded, in bytes
	 */
	void upload_data(void const *p_data, unsigned long const p_num_bytes);
	/**
	 * Creates or resizes the buffer object with the given number of bytes.
	 * Existing contents will be erased by this call.
	 * This is mainly useful if the buffer_object_mapping class is used.
	 *
	 * @param p_num_bytes New size for the buffer object, in bytes
	 */
	void resize(unsigned long const p_num_bytes);

	/**
	 * Returns the size of the buffer object, in bytes
	 */
	inline unsigned long get_size() const
	{
		return m_size; // also used in buffer_object_mapping in assert() expressions to catch out-of-bound errors
	}

protected:
	GLuint m_name;
	GLenum m_target, m_usage;
	unsigned long m_size;
};


/**
 * RAII-style class for mapping the buffer object contents to the local address space.
 * Example usage:
 * @code{.cpp}
 *   buffer_object buf(GL_ARRAY_BUFFER, GL_STATIC_DRAW);
 *   buf.resize(4096);
 *   {
 *     buffer_object_mapping map(buf, GL_MAP_WRITE_BIT);
 *     void *ptr = map.get_mapped_pointer();
 *
 *     // .... do something with the data pointed at by ptr ....
 *
 *     // buffer_object_mapping destructor automatically unmaps the pointer
 *   }
 * @endcode
 */
class buffer_object_mapping:
	private noncopyable
{
public:
	/**
	 * Constructor. Maps the entire buffer object data to the local address space using glMapBufferRange().
	 * @param p_bufobj Buffer object whose data shall be mapped
	 * @param p_access See the OpenGL glMapBufferRange() documentation for an explanation of this value
	 */
	explicit buffer_object_mapping(buffer_object &p_bufobj, GLenum const p_access);
	/**
	 * Constructor. Maps a subset of the buffer object data to the local address space using glMapBufferRange().
	 * @param p_bufobj Buffer object whose data shall be mapped
	 * @param p_access See the OpenGL glMapBufferRange() documentation for an explanation of this value
	 * @param p_offset Offset within the buffer object data, in bytes; the mapping will start at this offset
	 * @param p_length Length of the buffer object data to map, in bytes; the mapping will be valid for this amount of bytes
	 */
	explicit buffer_object_mapping(buffer_object &p_bufobj, GLenum const p_access, GLintptr const p_offset, GLsizeiptr p_length);
	/**
	 * Destructor. Unmaps the buffer object data by using glUnmapBuffer().
	 */
	~buffer_object_mapping();

	/**
	 * Returns the pointer to the mapped data.
	 */
	void* get_mapped_pointer();
	/**
	 * Returns the pointer to the mapped data.
	 */
	void const * get_mapped_pointer() const;

	/**
	 * Explicitely flush a subrange. See the OpenGL glMapBufferRange() documentation for details about explicit flushing.
	 * This call is necessary only if explicit flushing was enabled in the constructor's access argument.
	 * @param p_offset Offset, in bytes, of the region to flush
	 * @param p_length Length, in bytes, of the region to flush
	 */
	void flush_subrange(GLintptr const p_offset, GLsizeiptr p_length);
	/**
	 * Explicitely flush the entire mapped range. See the OpenGL glMapBufferRange() documentation for details about explicit flushing.
	 * This call is necessary only if explicit flushing was enabled in the constructor's access argument.
	 */
	void flush_entire_range();

private:
	buffer_object &m_bufobj;
	GLvoid *m_mapped_pointer;
	GLintptr m_flush_offset;
	GLsizeiptr m_flush_length;
	bool m_explicit_flush;
};


// glBindBufferBase
void bind_to_indexed_target(buffer_object &p_bufobj, GLenum const p_target, GLuint const p_index);
// glBindBufferRange
void bind_to_indexed_target(buffer_object &p_bufobj, GLenum const p_target, GLuint const p_index, GLintptr const p_offset, GLsizeiptr const p_size);


}


#endif

