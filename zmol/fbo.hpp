/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_FBO_HPP
#define ZMOL_FBO_HPP

#include <algorithm>
#include <vector>
#include "opengl3.hpp"
#include "texture.hpp"
#include "noncopyable.hpp"


namespace zmol
{


/**
 * Framebuffer object wrapper class.
 *
 * The class creates a framebuffer object in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 */
 class fbo:
	private noncopyable
{
public:
	/**
	 * Constructor. Creates the framebuffer object using glGenFramebuffers().
	 *
	 * @param p_target Target. Can be GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER or GL_FRAMEBUFFER (the default).
	 */
	explicit fbo(GLenum const p_target = GL_FRAMEBUFFER);
	/**
	 * Destructor. Destroys the framebuffer object using glDeleteFramebuffers().
	 */
	~fbo();

	/**
	 * Returns the OpenGL name of the framebuffer object.
	 */
	inline GLuint get_name() const
	{
		return m_name;
	}

	/**
	 * Returns the target of the framebuffer object.
	 */
	inline GLuint get_target() const
	{
		return m_target;
	}

	/**
	 * Binds the framebuffer object to the OpenGL context.
	 */
	void bind();
	/**
	 * Unbinds the framebuffer object to the OpenGL context.
	 */
	void unbind();

protected:
	GLuint m_name;
	GLenum m_target;
};


/**
 * Renderbuffer wrapper class.
 *
 * The class creates a renderbuffer in the constructor, and destroys it in the destructor.
 * This makes proper use of the C++ RAII idiom, and ensures exception safety (-> no resource leak).
 */
class renderbuffer:
	private noncopyable
{
public:
	/**
	 * Constructor. Creates the renderbuffer using glGenRenderbuffers().
	 *
	 * @param p_format Pixel format of the renderbuffer.
	 * @param p_width Width of the renderbuffer, in pixels.
	 * @param p_height Height of the renderbuffer, in pixels.
	 * @param p_num_antialiasing_samples Number of antialiasing samples. Set this to 0 to disable antialiasing in this renderbuffer.
	 */
	explicit renderbuffer(GLenum const p_format, GLsizei const p_width, GLsizei const p_height, GLsizei const p_num_antialiasing_samples);
	/**
	 * Destructor. Destroys the renderbuffer using glDeleteRenderbuffers().
	 */
	~renderbuffer();

	/**
	 * Returns the target of the renderbuffer.
	 */
	inline GLuint get_name() const
	{
		return m_name;
	}

	/**
	 * Binds the renderbuffer to the OpenGL context.
	 */
	void bind();
	/**
	 * Unbinds the renderbuffer to the OpenGL context.
	 */
	void unbind();

protected:
	GLuint m_name;

};


/**
 * Attaches the given renderbuffer to the given framebuffer object at the specified attachment point.
 * @param p_fbo Framebuffer object to attach to
 * @param p_attachment The attachment point to use
 * @param p_renderbuf Renderbuffer to attach
 */
void attach_to(fbo &p_fbo, GLenum const p_attachment, renderbuffer const &p_renderbuf);
/**
 * Attaches the given texture to the given framebuffer object at the specified attachment point.
 * @param p_fbo Framebuffer object to attach to
 * @param p_attachment The attachment point to use
 * @param p_texture Texture to attach
 */
void attach_to(fbo &p_fbo, GLenum const p_attachment, texture const &p_texture);
/**
 * Detaches the given renderbuffer from the given framebuffer object at the specified attachment point.
 * @param p_fbo Framebuffer object to detach from
 * @param p_attachment The attachment point
 * @param p_renderbuf Renderbuffer to detach
 */
void detach_from(fbo &p_fbo, GLenum const p_attachment, renderbuffer const &p_renderbuf);
/**
 * Detaches the given texture from the given framebuffer object at the specified attachment point.
 * @param p_fbo Framebuffer object to detach from
 * @param p_attachment The attachment point
 * @param p_texture Texture to detach
 */
void detach_from(fbo &p_fbo, GLenum const p_attachment, texture const &p_texture);


/**
 * Set the draw buffer to use. This can be used to explicitely draw to a certain attachment point.
 * @param p_buffer Buffer to draw to.
 */
void set_draw_buffer(GLenum const p_buffer);
/**
 * Sets the draw buffers to use. This can be used to explicitely draw to certain attachment points
 * (= "multiple render-to-texture").
 * @param p_num_buffers Number of buffers in the buffer index array.
 * @param p_buffers Array of buffer indices.
 */
void set_draw_buffers(GLsizei const p_num_buffers, GLenum const * p_buffers);


namespace detail
{


template < typename Buffers >
struct set_draw_buffers_impl
{
	inline void operator()(Buffers const &p_buffers) const
	{
		std::vector < GLenum > buffers_array;
		std::copy(p_buffers.begin(), p_buffers.end(), std::back_inserter(buffers_array));
		set_draw_buffers(buffers_array.size(), &buffers_array[0]);
	}
};


template < typename T, typename Alloc >
struct set_draw_buffers_impl < std::vector < T, Alloc > >
{
	inline void operator()(std::vector < T, Alloc > const &p_buffers) const
	{
		set_draw_buffers(p_buffers.size(), &p_buffers[0]);
	}
};


}


/**
 * Sets the draw buffers to use. This can be used to explicitely draw to certain attachment points
 * (= "multiple render-to-texture").
 * @param p_buffers Buffer to draw to. The type of buffers is expected to be a STL compliant sequence
 * of GLenum values (the buffer indices).
 */
template < typename Buffers >
inline void set_draw_buffers(Buffers const &p_buffers)
{
	detail::set_draw_buffers_impl < Buffers > ()(p_buffers);
}


}


#endif

