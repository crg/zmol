/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include <assert.h>
#include "buffer_object.hpp"


namespace zmol 
{


buffer_object::buffer_object(GLenum const p_target, GLenum const p_usage):
	m_name(0),
	m_target(p_target),
	m_usage(p_usage),
	m_size(0)
{
	glGenBuffers(1, &m_name);
}


buffer_object::~buffer_object()
{
	unbind();
	if (m_name != 0)
		glDeleteBuffers(1, &m_name);
}


void buffer_object::bind()
{
	glBindBuffer(m_target, m_name);
}


void buffer_object::unbind()
{
	glBindBuffer(m_target, 0);
}


void buffer_object::upload_data(void const *p_data, unsigned long const p_num_bytes)
{
	glBufferData(m_target, p_num_bytes, p_data, m_usage);
	m_size = p_num_bytes;
}


void buffer_object::resize(unsigned long const p_num_bytes)
{
	glBufferData(m_target, p_num_bytes, 0, m_usage);
	m_size = p_num_bytes;
}



buffer_object_mapping::buffer_object_mapping(buffer_object &p_bufobj, GLenum const p_access):
	m_bufobj(p_bufobj),
	m_mapped_pointer(0)
{
	assert(p_bufobj.get_size() > 0);
	m_mapped_pointer = glMapBufferRange(p_bufobj.get_target(), 0, p_bufobj.get_size(), p_access);
	if ((p_access & GL_MAP_FLUSH_EXPLICIT_BIT) != 0)
	{
		m_flush_offset = 0;
		m_flush_length = p_bufobj.get_size();
		m_explicit_flush = true;
	}
	else
		m_explicit_flush = false;
}


buffer_object_mapping::buffer_object_mapping(buffer_object &p_bufobj, GLenum const p_access, GLintptr const p_offset, GLsizeiptr p_length):
	m_bufobj(p_bufobj),
	m_mapped_pointer(0)
{
	assert(p_bufobj.get_size() > 0);
	assert(long(p_offset + p_length) <= long(p_bufobj.get_size()));
	m_mapped_pointer = glMapBufferRange(p_bufobj.get_target(), p_offset, p_length, p_access);
	if ((p_access & GL_MAP_FLUSH_EXPLICIT_BIT) != 0)
	{
		m_flush_offset = p_offset;
		m_flush_length = p_length;
		m_explicit_flush = true;
	}
	else
		m_explicit_flush = false;
}


buffer_object_mapping::~buffer_object_mapping()
{
	glUnmapBuffer(m_bufobj.get_target());
}


void* buffer_object_mapping::get_mapped_pointer()
{
	return m_mapped_pointer;
}


void const * buffer_object_mapping::get_mapped_pointer() const
{
	return m_mapped_pointer;
}


void buffer_object_mapping::flush_subrange(GLintptr const p_offset, GLsizeiptr p_length)
{
	if (m_explicit_flush)
		glFlushMappedBufferRange(m_bufobj.get_target(), p_offset, p_length);
}


void buffer_object_mapping::flush_entire_range()
{
	flush_subrange(m_flush_offset, m_flush_length);
}



void bind_to_indexed_target(buffer_object &p_bufobj, GLenum const p_target, GLuint const p_index)
{
	glBindBufferBase(p_target, p_index, p_bufobj.get_name());
}


void bind_to_indexed_target(buffer_object &p_bufobj, GLenum const p_target, GLuint const p_index, GLintptr const p_offset, GLsizeiptr const p_size)
{
	glBindBufferRange(p_target, p_index, p_bufobj.get_name(), p_offset, p_size);
}


}

