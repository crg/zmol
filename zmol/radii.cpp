/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include "radii.hpp"


namespace zmol
{


namespace
{


struct radius_entry
{
	char const * m_name;
	float m_radius, m_covalent_radius;
};


radius_entry const radius_entries[] =
{
	{ " F  ", 1.470f, 1.000f },
	{ "CL  ", 1.890f, 1.000f },
	{ " H  ", 1.100f, 0.320f },
	{ " C  ", 1.548f, 0.720f },
	{ " N  ", 1.400f, 0.680f },
	{ " O  ", 1.348f, 0.680f },
	{ " P  ", 1.880f, 1.036f },
	{ " S  ", 1.808f, 1.020f },
	{ "CA  ", 1.948f, 0.992f },
	{ "FE  ", 1.948f, 1.420f },
	{ "ZN  ", 1.148f, 1.448f },
	{ "CD  ", 1.748f, 1.688f },
	{ " I  ", 1.748f, 1.400f },

	{ 0, 0, 0 }
};


}


radii::radii()
{
	for (radius_entry const *entry = radius_entries; entry->m_name != 0; ++entry)
		m_radii[entry->m_name] = cml::vector2f(entry->m_radius, entry->m_covalent_radius);
}


float radii::get_radius(std::string const &p_name) const
{
	auto radius_iter = m_radii.find(p_name);
	if (radius_iter == m_radii.end())
		return 1.0f;
	else
		return radius_iter->second[0];
}


float radii::get_covalent_radius(std::string const &p_name) const
{
	auto radius_iter = m_radii.find(p_name);
	if (radius_iter == m_radii.end())
		return 1.0f;
	else
		return radius_iter->second[1];
}


}

