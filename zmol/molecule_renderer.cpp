/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifdef WIN32

#ifndef NOMINMAX
#define NOMINMAX
#endif

#endif

#include <assert.h>
#include <cstdint>
#include <cmath>
#include "fbo.hpp"
#include "molecule_renderer.hpp"
#include "directions.hpp"
#include "colors.hpp"
#include "radii.hpp"


namespace zmol
{


molecule_shaders::program_data::program_data():
	m_model_view_uniform(-1),
	m_projection_uniform(-1),
	m_scale_factor_uniform(-1),
	m_texlen_uniform(-1),
	m_border_and_flatness_uniform(-1),
	m_lighting_params_uniform(-1)
{
}


molecule_shaders::molecule_shaders(std::function < void(std::string const &p_errormsg) > const &p_errormsg_callback)
{
	/*
	All GLSL programs here are initialized in the same fashion:
	1. Program is built out of specified vertex and fragment shaders, with a user-defined error message callback
	   for displaying compilation and linkage errors
	2. Vertex shader input names are bound to attribute indices
	3. Program is linked (MUST happen before step 2 !!)
	4. Uniform locations are retrieved
	5. Fragment data location (= fragment shader output) is specified
	6. Samplers are assigned texture unit indices

	Note that for most shaders, not all uniform locations are retrieved.
	*/



	/////
	///// SPHERE SHADERS


	/// Shaders for drawing the textured spheres to the screen

	build_program(m_sphere_draw_to_screen.m_program, p_errormsg_callback)
		(
		GL_VERTEX_SHADER, "sphere_draw_to_screen_vs",
"#version 150\n"
"\n"
"in vec3 position;\n"
"in vec2 st;\n"
"in vec4 color_radius;\n"
"in vec2 uv;\n"
"in vec4 uvquad;\n"
"\n"
"invariant out vec2 st_vs;\n"
"invariant out vec2 uv_vs;\n"
"invariant out vec4 uvquad_vs;\n"
"invariant out vec3 color_vs;\n"
"invariant out float scaled_radius_vs;\n"
"invariant out float unscaled_radius_vs;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform float scale_factor;\n"
"uniform vec3 border_and_flatness; // x = border with  y = border variance  z = flatness\n"
"\n"
"void main(void)\n"
"{\n"
"	float scaled_radius = scale_factor * color_radius.a;\n"
"	vec2 st2 = st * (1.0 + border_and_flatness.x / color_radius.a);\n"
"	vec4 pos = model_view * vec4(position * scale_factor, 1.0) + vec4(st2 * scaled_radius, 0.0, 0.0);\n"
"	gl_Position = projection * pos;\n"
"	st_vs = st2;\n"
"	uv_vs = uv;\n"
"	uvquad_vs = uvquad;\n"
"	color_vs = color_radius.rgb;\n"
"	scaled_radius_vs = scaled_radius;\n"
"	unscaled_radius_vs = color_radius.a;\n"
"}\n"
		)
		(
		GL_FRAGMENT_SHADER, "sphere_draw_to_screen_fs",
"#version 150\n"
"\n"
"invariant in vec2 st_vs;\n"
"invariant in vec2 uv_vs;\n"
"invariant in vec4 uvquad_vs;\n"
"invariant in vec3 color_vs;\n"
"invariant in float scaled_radius_vs;\n"
"invariant in float unscaled_radius_vs;\n"
"\n"
"out vec4 color;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform sampler2D ao_atlas;\n"
"uniform float texlen;\n"
"/*uniform float border_width;\n"
"uniform float border_variance;*/\n"
"uniform vec3 border_and_flatness; // x = border with  y = border variance  z = flatness\n"
"uniform vec4 lighting_params;\n"
"\n"
"void main(void)\n"
"{\n"
"	float border_width = border_and_flatness.x;\n"
"	float border_variance = border_and_flatness.y;\n"
"	float flatness = border_and_flatness.z;\n"
"\n"
"	float dist = dot(st_vs, st_vs);\n"
"	if (dist > (1.0 + border_width / unscaled_radius_vs))\n"
"	{\n"
"		discard;\n"
"	}\n"
"	else if (dist > 1.0)\n"
"	{\n"
"		color = vec4(0,0,0,1);\n"
"		gl_FragDepth = gl_FragCoord.z + (dist - 1.0) * (4.0 * border_variance) / (border_width * 10.0) * 0.5 * projection[2][2]; // *0.5 to compensate for the 2 factor in the projection matrix\n"
"	}\n"
"	else\n"
"	{\n"
"		float dz = sqrt(max(0, 1.0 - dist));\n"
"		gl_FragDepth = gl_FragCoord.z - dz * 0.5 * scaled_radius_vs * projection[2][2]; // *0.5 to compensate for the 2 factor in the projection matrix\n"
"		\n"
"		vec3 n = vec3(st_vs, -dz);\n"
"		mat3 transp_mv = transpose(mat3(model_view));\n"
"		n = normalize(transp_mv * n);\n"
"		vec3 absn = abs(n);\n"
"		float d = absn.x + absn.y + absn.z;\n"
"		vec2 uv_ao;\n"
"		if (n.z <= 0) {\n"
"			uv_ao = n.xy / d;\n"
"		} else {\n"
"			vec2 sgn = n.xy / absn.xy;\n"
"			uv_ao = sgn - absn.yx * (sgn / d);\n"
"		}\n"
"		uv_ao = (uv_ao * 0.5) + 0.5;\n"
"		\n"
"		uv_ao.x = mix(uvquad_vs.x, uvquad_vs.y - 1.0 / texlen, uv_ao.x);\n"
"		uv_ao.y = mix(uvquad_vs.z, uvquad_vs.w - 1.0 / texlen, uv_ao.y);\n"
"		float ao = 1.0 - texture2D(ao_atlas, uv_ao).r;\n"
"		vec3 lightdir = transp_mv * vec3(0, 0.6, -0.8);\n"
"		float ndotl = max(0.0, dot(n, lightdir)) * (1.0 - flatness) + flatness;\n"
"		float ndoth = pow(ndotl, lighting_params.w * 50.0 + 1.0) * lighting_params.z * (1.0 - (1.0 - ao) * lighting_params.x);\n"
"\n"
"		color = vec4((ao * lighting_params.x + ndotl * lighting_params.y) * color_vs + ndoth * vec3(1.0, 1.0, 1.0), 1.0);\n"
"	}\n"
"}\n"
		);

	// Bind vertex shader input names to attribute locations 
	// Bindings must happen BEFORE linking!
	glBindAttribLocation(m_sphere_draw_to_screen.m_program.get_name(), 0, "position");
	glBindAttribLocation(m_sphere_draw_to_screen.m_program.get_name(), 1, "st");
	glBindAttribLocation(m_sphere_draw_to_screen.m_program.get_name(), 2, "color_radius");
	glBindAttribLocation(m_sphere_draw_to_screen.m_program.get_name(), 3, "uv");
	glBindAttribLocation(m_sphere_draw_to_screen.m_program.get_name(), 4, "uvquad");

	// Link the program
	m_sphere_draw_to_screen.m_program.link();

	// Retrieve uniform locations
	m_sphere_draw_to_screen.m_model_view_uniform = m_sphere_draw_to_screen.m_program.get_uniform_location("model_view");
	m_sphere_draw_to_screen.m_projection_uniform = m_sphere_draw_to_screen.m_program.get_uniform_location("projection");
	m_sphere_draw_to_screen.m_scale_factor_uniform = m_sphere_draw_to_screen.m_program.get_uniform_location("scale_factor");
	m_sphere_draw_to_screen.m_texlen_uniform = m_sphere_draw_to_screen.m_program.get_uniform_location("texlen");
	m_sphere_draw_to_screen.m_border_and_flatness_uniform = m_sphere_draw_to_screen.m_program.get_uniform_location("border_and_flatness");
	m_sphere_draw_to_screen.m_lighting_params_uniform = m_sphere_draw_to_screen.m_program.get_uniform_location("lighting_params");

	// Bind the fragment data location
	glBindFragDataLocation(m_sphere_draw_to_screen.m_program.get_name(), 0, "color");

	// Assign texture unit indices to samplers
	m_sphere_draw_to_screen.m_program.bind();
	glUniform1i(m_sphere_draw_to_screen.m_program.get_uniform_location("ao_atlas"), 0);
	m_sphere_draw_to_screen.m_program.unbind();


	/// Shaders for drawing the spheres to the shadowmap

	build_program(m_sphere_draw_to_shadowmap.m_program, p_errormsg_callback)
		(
		GL_VERTEX_SHADER, "sphere_draw_to_shadowmap_vs",
"#version 150\n"
"\n"
"in vec3 position;\n"
"in vec2 st;\n"
"in vec4 color_radius;\n"
"\n"
"invariant out vec2 st_vs;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform float scale_factor;\n"
"\n"
"void main(void)\n"
"{\n"
"	vec4 pos = model_view * vec4(position * scale_factor, 1.0) + vec4(st * scale_factor * color_radius.a, 0.0, 0.0);\n"
"	gl_Position = projection * pos;\n"
"	st_vs = st;\n"
"}\n"
		)
		(
		GL_FRAGMENT_SHADER, "sphere_draw_to_shadowmap_fs",
"#version 150\n"
"\n"
"invariant in vec2 st_vs;\n"
"\n"
"void main(void)\n"
"{\n"
"	float d = dot(st_vs, st_vs);\n"
"	if (d > 1.0) discard;\n"
"}\n"
		);

	// Bind vertex shader input names to attribute locations 
	// Bindings must happen BEFORE linking!
	glBindAttribLocation(m_sphere_draw_to_shadowmap.m_program.get_name(), 0, "position");
	glBindAttribLocation(m_sphere_draw_to_shadowmap.m_program.get_name(), 1, "st");
	glBindAttribLocation(m_sphere_draw_to_shadowmap.m_program.get_name(), 2, "color_radius");

	// Link the program
	m_sphere_draw_to_shadowmap.m_program.link();

	// Retrieve uniform locations
	m_sphere_draw_to_shadowmap.m_model_view_uniform = m_sphere_draw_to_shadowmap.m_program.get_uniform_location("model_view");
	m_sphere_draw_to_shadowmap.m_projection_uniform = m_sphere_draw_to_shadowmap.m_program.get_uniform_location("projection");
	m_sphere_draw_to_shadowmap.m_scale_factor_uniform = m_sphere_draw_to_shadowmap.m_program.get_uniform_location("scale_factor");

	// This program's fragment shader has no fragment data location, because it outputs no color values,
	// just depth ones


	/// Shaders for drawing shadowed spheres to the atlas
	/// (Using the shadowmap computed by the shaders above)

	build_program(m_sphere_draw_to_ao_atlas.m_program, p_errormsg_callback)
		(
		GL_VERTEX_SHADER, "sphere_draw_to_ao_atlas_vs",
"#version 150\n"
"\n"
"in vec3 position;\n"
"in vec2 st;\n"
"in vec4 color_radius;\n"
"in vec2 uv;\n"
"in vec4 uvquad;\n"
"\n"
"invariant out vec2 st_vs;\n"
"invariant out vec2 uv_vs;\n"
"invariant out vec3 pos;\n"
"invariant out float scaled_radius_vs;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform float scale_factor;\n"
"\n"
"void main(void)\n"
"{\n"
"	float scaled_radius = scale_factor * color_radius.a;\n"
"	vec4 p = vec4(uv * 2.0 - 1.0, 0.0, 1.0);\n"
"	gl_Position = p;\n"
"	pos = ((model_view * vec4(position * scale_factor, 1.0))).xyz;\n"
"	uv_vs = uv;\n"
"	st_vs = st;\n"
"	scaled_radius_vs = scaled_radius;\n"
"}\n"
		)
		(
		GL_FRAGMENT_SHADER, "sphere_draw_to_ao_atlas_fs",
"#version 150\n"
"\n"
"in vec2 st_vs;\n"
"invariant in vec2 uv_vs;\n"
"invariant in vec3 pos;\n"
"invariant in float scaled_radius_vs;\n"
"\n"
"out vec4 color;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform sampler2D shadowmap;\n"
"\n"
"void main(void)\n"
"{\n"
"	vec2 abs_st = abs(st_vs);\n"
"	float h = 1 - abs_st.x - abs_st.y;\n"
"	vec3 spherepos;\n"
"	if (h >= 0)\n"
"		spherepos = vec3(st_vs, -h);\n"
"	else\n"
"		spherepos = vec3(sign(st_vs.x) * (1 - abs_st.y) , sign(st_vs.y) * (1 - abs_st.x) , -h);\n"
"	spherepos = normalize((mat3(model_view)) * spherepos) * scaled_radius_vs;\n"
"	if (spherepos.z > 0) discard;\n"
"	spherepos.z *= 0.5 * projection[2][2];\n"
"	vec3 curpos = (projection * vec4(pos,1.0)).xyz + spherepos;\n"
"	float shadowmap_z = texture2D(shadowmap, (pos.xy + spherepos.xy) * 0.5 + 0.5).r * 2.0 - 1.0;\n"
"	color = vec4(vec3(1,1,1) * 1.0/81.0 * ((shadowmap_z <= curpos.z) ? 0.0 : 1.0) , 0.0);\n"
"}\n"
		);

	// Bind vertex shader input names to attribute locations 
	// Bindings must happen BEFORE linking!
	glBindAttribLocation(m_sphere_draw_to_ao_atlas.m_program.get_name(), 0, "position");
	glBindAttribLocation(m_sphere_draw_to_ao_atlas.m_program.get_name(), 1, "st");
	glBindAttribLocation(m_sphere_draw_to_ao_atlas.m_program.get_name(), 2, "color_radius");
	glBindAttribLocation(m_sphere_draw_to_ao_atlas.m_program.get_name(), 3, "uv");
	glBindAttribLocation(m_sphere_draw_to_ao_atlas.m_program.get_name(), 4, "uvquad");

	// Link the program
	m_sphere_draw_to_ao_atlas.m_program.link();

	// Retrieve uniform locations
	m_sphere_draw_to_ao_atlas.m_model_view_uniform = m_sphere_draw_to_ao_atlas.m_program.get_uniform_location("model_view");
	m_sphere_draw_to_ao_atlas.m_projection_uniform = m_sphere_draw_to_ao_atlas.m_program.get_uniform_location("projection");
	m_sphere_draw_to_ao_atlas.m_scale_factor_uniform = m_sphere_draw_to_ao_atlas.m_program.get_uniform_location("scale_factor");

	// Bind the fragment data location
	glBindFragDataLocation(m_sphere_draw_to_ao_atlas.m_program.get_name(), 0, "color");

	// Assign texture unit indices to samplers
	m_sphere_draw_to_ao_atlas.m_program.bind();
	glUniform1i(m_sphere_draw_to_ao_atlas.m_program.get_uniform_location("shadowmap"), 0);
	m_sphere_draw_to_ao_atlas.m_program.unbind();



	/////
	///// CYLINDER SHADERS


	/// Shaders for drawing the textured cylinders to the screen
	// TODO: these shaders are unfinished; do not use yet

	build_program(m_cylinder_draw_to_screen.m_program, p_errormsg_callback)
		(
		GL_VERTEX_SHADER, "cylinder_draw_to_screen_vs",
"#version 150\n"
"\n"
"in vec3 position;\n"
"in vec2 st;\n"
"in vec4 color_radius;\n"
"in vec2 uv;\n"
"in vec4 uvquad;\n"
"in vec3 dir;\n"
"\n"
"invariant out vec2 rfac;\n"
"invariant out float ndispend;\n"
"invariant out float nrdispend;\n"
"invariant out float dofs;\n"
"invariant out float ndofs;\n"
"invariant out vec2 st_vs;\n"
"invariant out vec4 uvquad_vs;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform float scale_factor;\n"
"\n"
"void main(void)\n"
"{\n"
"	float radius = color_radius.a;\n"
"	float cylrad = scale_factor * radius;\n"
"\n"
"	vec4 transf_pos = model_view * vec4(position * scale_factor, 1.0);\n"
"	vec4 transf_other_pos = model_view * vec4((position + dir) * scale_factor, 1.0);\n"
"	vec3 transf_dir = (transf_other_pos - transf_pos).xyz;\n"
"	float cyllen = length(transf_dir);\n"
"	float vcyllen = length(transf_dir.xy);\n"
"	rfac = transf_dir.xy / vcyllen;\n"
"	float dispend = cylrad * (transf_other_pos.z - transf_pos.z) / cyllen;\n"
"	ndispend = dispend / vcyllen;\n"
"	nrdispend = dispend / cylrad;\n"
"	vec3 dispdir = vec3(rfac * dispend, transf_dir.z * dispend / cyllen);\n"
"	vec2 ntdir2 = normalize(transf_dir.xy);\n"
"	dofs = cylrad * cyllen * inversesqrt(cyllen * cyllen - (transf_other_pos.z - transf_pos.z) * (transf_other_pos.z - transf_pos.z));\n"
"	dofs = clamp(dofs, 0.0, 2.0 * cylrad);\n"
"	ndofs = dofs / cylrad;\n"
"\n"
"	vec3 viewdisp;\n"
"	if ((dispend * st.t) > 0)\n"
"	{\n"
"		viewdisp = vec3(st.x * ntdir2.y * cylrad + dispdir.x, -st.x * ntdir2.x * cylrad + dispdir.y, -dispdir.z);\n"
"		st_vs = vec2(st.s, st.t + ndispend);\n"
"	}\n"
"	else\n"
"	{\n"
"		viewdisp = vec3(st.x * ntdir2.y * cylrad, -st.x * ntdir2.x * cylrad, 0.0);\n"
"		st_vs = st;\n"
"	}\n"
"	uvquad_vs = uvquad;\n"
"	gl_Position = projection * (vec4(transf_pos.xyz + viewdisp, 1.0) + vec4(0,0,0,0));\n"
"}\n"
		)
		(
		GL_FRAGMENT_SHADER, "cylinder_draw_to_screen_fs",
"#version 150\n"
"\n"
"invariant in vec2 rfac;\n"
"invariant in float ndispend;\n"
"invariant in float nrdispend;\n"
"invariant in float dofs;\n"
"invariant in float ndofs;\n"
"invariant in vec2 st_vs;\n"
"invariant in vec4 uvquad_vs;\n"
"\n"
"uniform mat4 model_view;\n"
"uniform mat4 projection;\n"
"uniform sampler2D ao_atlas;\n"
"uniform float texlen;\n"
"\n"
"out vec4 color;\n"
"\n"
"void main(void)\n"
"{\n"
"	float axadj = sqrt(1.0 - st_vs.s * st_vs.s);\n"
"	float dispcurv = ndispend * axadj;\n"
"\n"
"	float d = dofs * (axadj) * 0.5 * projection[2][2];\n"
"	gl_FragDepth = gl_FragCoord.z - d;\n"
"\n"
"	vec3 norm = vec3(\n"
"		nrdispend * rfac.x * axadj + st_vs.s * rfac.y,\n"
"		-nrdispend * rfac.y * axadj + st_vs.s * rfac.x,\n"
"		ndofs * axadj\n"
"	);\n"
"	norm = normalize(norm);\n"
"	//norm = normalize(transpose(mat3(model_view)) * norm);\n"
"\n"
"	vec2  uv_ao = vec2(norm.y / (2.0 * (abs(norm.x) + abs(norm.y))), norm.z);\n"
"	if (norm.x <= 0)\n"
"		uv_ao.x = uv_ao.x - 0.5;\n"
"	else\n"
"		uv_ao.x = -uv_ao.x + 0.5;\n"
"	uv_ao.x = mix(uvquad_vs.x, uvquad_vs.y - 1.0 / texlen, uv_ao.x);\n"
"	uv_ao.y = mix(uvquad_vs.z, uvquad_vs.w - 1.0 / texlen, uv_ao.y);\n"
"\n"
"	if ((st_vs.t <= (-1.0 + dispcurv)) || (st_vs.t >= (1.0 + dispcurv)))\n"
"		discard;\n"
"//		color = vec4(1,0,0,1); else\n"
"\n"
"//	color = vec4(uv_ao, 0.0, 1.0);\n"
"	color = vec4(st_vs.s * 0.5 + 0.5, trunc((st_vs.t - dispcurv) * 10.0) / 10.0 * 0.5 + 0.5, 0.0, 1.0);\n"
"}\n"
		);

	// Bind vertex shader input names to attribute locations 
	// Attribute location bindings must happen BEFORE linking!
	glBindAttribLocation(m_cylinder_draw_to_screen.m_program.get_name(), 0, "position");
	glBindAttribLocation(m_cylinder_draw_to_screen.m_program.get_name(), 1, "st");
	glBindAttribLocation(m_cylinder_draw_to_screen.m_program.get_name(), 2, "color_radius");
	glBindAttribLocation(m_cylinder_draw_to_screen.m_program.get_name(), 3, "uv");
	glBindAttribLocation(m_cylinder_draw_to_screen.m_program.get_name(), 4, "uvquad");
	glBindAttribLocation(m_cylinder_draw_to_screen.m_program.get_name(), 5, "dir");

	// Link the program
	m_cylinder_draw_to_screen.m_program.link();

	// Retrieve uniform locations
	m_cylinder_draw_to_screen.m_model_view_uniform = m_cylinder_draw_to_screen.m_program.get_uniform_location("model_view");
	m_cylinder_draw_to_screen.m_projection_uniform = m_cylinder_draw_to_screen.m_program.get_uniform_location("projection");
	m_cylinder_draw_to_screen.m_scale_factor_uniform = m_cylinder_draw_to_screen.m_program.get_uniform_location("scale_factor");
	m_cylinder_draw_to_screen.m_texlen_uniform = m_cylinder_draw_to_screen.m_program.get_uniform_location("texlen");

	// Bind the fragment data location
	glBindFragDataLocation(m_cylinder_draw_to_screen.m_program.get_name(), 0, "color");

	// Assign texture unit indices to samplers
	m_cylinder_draw_to_screen.m_program.bind();
	glUniform1i(m_cylinder_draw_to_screen.m_program.get_uniform_location("ao_atlas"), 0);
	m_cylinder_draw_to_screen.m_program.unbind();
}



namespace
{


/*
Vertex structure. This same structure is also used for rendering; in other words,
the vertices stored in the GPU's VRAM use this very same layout.
The difference between uv, uvquad, and st is:
- st describes the vertex' position inside the quad, using  a local coordinate system.
  Top left corner of a quad is always (-1,-1), bottom right one is always (+1,+1).
  Therefore, the value of st is always either (-1,-1), (+1,-1), (-1,+1), or (+1,+1).
- uv describes the top UV coordinate of the current vertex, which can be any of the quad's corners.
- uvquad describes the UV coordinates of both the top left (= -1,-1) and bottom right (= +1,+1) corner of the quad.
*/
struct vertex
{
	cml::vector3f m_position; // XYZ 3D position
	cml::vector2f m_st; // Quad coordinates, always go from -1 to +1 in both quad dimensions
	cml::vector4f m_color_radius; // 4D vector, xyz describes the color, w the radius
	cml::vector2f m_uv; // UV texture coordinates
	cml::vector4f m_uvquad; // Stores UV coordinates of min and max corners of the quad
	cml::vector3f m_direction; // only for cylinders
};


bool rendering_mode_uses_cylinders(molecule_renderer::rendering_modes const p_mode)
{
	switch (p_mode)
	{
		case molecule_renderer::rendering_mode_space_filling: return false;
		case molecule_renderer::rendering_mode_balls_and_sticks: return true;
		case molecule_renderer::rendering_mode_licorice: return true;
	}

	assert(false); // If this is reached, then a new rendering mode was introduced, and not added to the switch above
	return false; // Should never be reached; this is placed here just to shut up the compiler
}


}


molecule_renderer::molecule_renderer(
	molecule_shaders &p_molecule_shaders,
	molecule_data const &p_data,
	unsigned int const p_initial_left_viewport_offset, unsigned int const p_initial_top_viewport_offset,
	unsigned int const p_initial_viewport_width, unsigned int const p_initial_viewport_height,
	unsigned int const p_initial_atlas_size,
	float const p_initial_radius_scale,
	rendering_modes const p_initial_rendering_mode,
	atom_color_modes const p_initial_atom_color_mode
):
	m_shaders(p_molecule_shaders),
	m_vertices(GL_ARRAY_BUFFER, GL_STATIC_DRAW),
	m_indices(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW),
	m_data(p_data), // this performs the internal copy of p_data
	m_num_atoms(m_data.m_atoms.size()),
	m_num_bonds(m_data.m_bonds.size()),
	m_radius_scale(p_initial_radius_scale),
	m_border_width(0.0f),
	m_border_variance(1.0f),
	m_ao_strength(1.0f),
	m_direct_lighting_strength(0.0f),
	m_shininess(0.0f),
	m_glossiness(0.0f),
	m_flatness(0.0f),
	m_left_viewport_offset(p_initial_left_viewport_offset),
	m_top_viewport_offset(p_initial_top_viewport_offset),
	m_viewport_width(p_initial_viewport_width),
	m_viewport_height(p_initial_viewport_height),
	m_atlas_size(p_initial_atlas_size),
	m_rendering_mode(p_initial_rendering_mode),
	m_atom_color_mode(p_initial_atom_color_mode)
{
	init_renderer();
}


molecule_renderer::molecule_renderer(
	molecule_shaders &p_molecule_shaders,
	molecule_data &&p_data,
	unsigned int const p_initial_left_viewport_offset, unsigned int const p_initial_top_viewport_offset,
	unsigned int const p_initial_viewport_width, unsigned int const p_initial_viewport_height,
	unsigned int const p_initial_atlas_size,
	float const p_initial_radius_scale,
	rendering_modes const p_initial_rendering_mode,
	atom_color_modes const p_initial_atom_color_mode
):
	m_shaders(p_molecule_shaders),
	m_vertices(GL_ARRAY_BUFFER, GL_STATIC_DRAW),
	m_indices(GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW),
	m_data(std::move(p_data)), // this moves the contents of p_data into m_data
	m_num_atoms(m_data.m_atoms.size()),
	m_num_bonds(m_data.m_bonds.size()),
	m_radius_scale(p_initial_radius_scale),
	m_border_width(0.0f),
	m_border_variance(1.0f),
	m_ao_strength(1.0f),
	m_direct_lighting_strength(0.0f),
	m_shininess(0.0f),
	m_glossiness(0.0f),
	m_flatness(0.0f),
	m_left_viewport_offset(p_initial_left_viewport_offset),
	m_top_viewport_offset(p_initial_top_viewport_offset),
	m_viewport_width(p_initial_viewport_width),
	m_viewport_height(p_initial_viewport_height),
	m_atlas_size(p_initial_atlas_size),
	m_rendering_mode(p_initial_rendering_mode),
	m_atom_color_mode(p_initial_atom_color_mode)
{
	init_renderer();
}


void molecule_renderer::init_renderer()
{
	calc_quads();

	// Set up the VAO. This needs to be done only once, even when the impostor geometry is recalculated.
	m_vao.bind();
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast < GLvoid* > (0));
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast < GLvoid* > (sizeof(float) * 3));
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast < GLvoid* > (sizeof(float) * 5));
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast < GLvoid* > (sizeof(float) * 9));
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast < GLvoid* > (sizeof(float) * 11));
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast < GLvoid* > (sizeof(float) * 15));
	m_vao.unbind();

	init_atlas_texture();
	calc_ao();
	set_viewport(m_left_viewport_offset, m_top_viewport_offset, m_viewport_width, m_viewport_height);
}


void molecule_renderer::set_rendering_mode(rendering_modes const p_new_rendering_mode)
{
	if (m_rendering_mode == p_new_rendering_mode)
		return;

	m_rendering_mode = p_new_rendering_mode;

	// First calculate the quads (= the impostor geometry)
	calc_quads();
	// ... then the AO (not the other way round)
	calc_ao();
}


void molecule_renderer::set_atom_color_mode(atom_color_modes const p_new_atom_color_mode)
{
	if (m_atom_color_mode == p_new_atom_color_mode)
		return;

	m_atom_color_mode = p_new_atom_color_mode;

	// Reinitialize the impostor geometry with the updated atom color mode,
	// but do not call calc_ao(), since the ambient occlusion is not affected by the atom color mode change
	calc_quads();
}


void molecule_renderer::set_viewport(unsigned int const p_new_left_viewport_offset, unsigned int const p_new_top_viewport_offset, unsigned int const p_new_viewport_width, unsigned int const p_new_viewport_height)
{
	float diameter = 2.0f / m_scale_factor;

	// If the viewport is shrunk in width or height, the rendering needs to be scaled accordingly.
	// Without this if expression, scaling would happen only in one of the two cases.
	// Also, the near and far planes are positioned to enclose the molecule.
	if (p_new_viewport_width < p_new_viewport_height)
	{
		float f = float(p_new_viewport_height) / float(p_new_viewport_width);
		cml::matrix_orthographic_LH(m_projection, -1.0f, +1.0f, -f, +f, 1.0f, diameter + 5.0f, cml::z_clip_neg_one);
	}
	else
	{
		float f = float(p_new_viewport_width) / float(p_new_viewport_height);
		cml::matrix_orthographic_LH(m_projection, -f, +f, -1.0f, +1.0f, 1.0f, diameter + 5.0f, cml::z_clip_neg_one);
	}

	m_left_viewport_offset = p_new_left_viewport_offset;
	m_top_viewport_offset = p_new_top_viewport_offset;
	m_viewport_width = p_new_viewport_width;
	m_viewport_height = p_new_viewport_height;

	// Set up OpenGL viewport.
	glViewport(p_new_left_viewport_offset, p_new_top_viewport_offset, p_new_viewport_width, p_new_viewport_height);
}


void molecule_renderer::set_radius_scale(float const p_new_radius_scale)
{
	m_radius_scale = p_new_radius_scale;

	// First calculate the quads (= the impostor geometry)
	calc_quads();
	// ... then the AO (not the other way round)
	calc_ao();
}


void molecule_renderer::set_border_width(float const p_new_border_width)
{
	m_border_width = p_new_border_width;
}


void molecule_renderer::set_ao_strength(float const p_new_ao_strength)
{
	m_ao_strength = p_new_ao_strength;
}


void molecule_renderer::set_direct_lighting_strength(float const p_new_direct_lighting_strength)
{
	m_direct_lighting_strength = p_new_direct_lighting_strength;
}


void molecule_renderer::set_shininess(float const p_new_shininess)
{
	m_shininess = p_new_shininess;
}


void molecule_renderer::set_glossiness(float const p_new_glossiness)
{
	m_glossiness = p_new_glossiness;
}


void molecule_renderer::set_flatness(float const p_new_flatness)
{
	m_flatness = p_new_flatness;
}


void molecule_renderer::set_border_variance(float const p_new_border_variance)
{
	m_border_variance = p_new_border_variance;
}


void molecule_renderer::set_atlas_size(unsigned int const p_new_atlas_size)
{
	if (p_new_atlas_size == m_atlas_size)
		return;

	m_atlas_size = p_new_atlas_size;

	// First calculate the quads (= the impostor geometry)
	init_atlas_texture();
	// ... then the AO (not the other way round)
	calc_ao();
}


void molecule_renderer::init_atlas_texture()
{
	// Create atlas texture. If another texture existed already, the old one will be destroyed.
	m_ao_atlas = texture_uptr(new texture(m_atlas_size, m_atlas_size, GL_R16, GL_RED, GL_UNSIGNED_SHORT));
}


void molecule_renderer::render(cml::quaternionf const &p_rotation)
{
	// Molecule is rendered here using the draw_to_screen shaders.

	// Convert rotation quaternion to 4x4 matrix
	matrix rotmat;
	cml::matrix_rotation_quaternion(rotmat, p_rotation);

	m_ao_atlas->bind(0);
	glEnable(GL_DEPTH_TEST);
	m_vao.bind();

	// First, render the spheres
	render_internal(m_shaders.m_sphere_draw_to_screen, rotmat, m_projection, pass_spheres);
	// Then, render the cylinders
	if (rendering_mode_uses_cylinders(m_rendering_mode))
		render_internal(m_shaders.m_cylinder_draw_to_screen, rotmat, m_projection, pass_cylinders);

	m_vao.unbind();
	m_ao_atlas->unbind(0);
}


void molecule_renderer::render_internal(molecule_shaders::program_data &p_program_data, matrix const &p_rotation, matrix const &p_projection, passes const p_pass)
{
	// Set up a translation matrix, using m_base_translation
	// (necessary to position the impostory geometry in front of the viewer, so it does not get clipped by the near plane)
	matrix trans;
	trans.identity();
	cml::matrix_translation(trans, m_base_translation);

	// Calculate combined translation and rotation matrix
	matrix modelview = trans * p_rotation;

	// Disable backface culling
	glDisable(GL_CULL_FACE);

	// Bind the GLSL program, and set its uniform matrix values
	// (modelview, projection, and scale_factor are always set, texlen only with some programs)
	p_program_data.m_program.bind();
	glUniformMatrix4fv(p_program_data.m_model_view_uniform, 1, GL_FALSE, modelview.data());
	glUniformMatrix4fv(p_program_data.m_projection_uniform, 1, GL_FALSE, p_projection.data());
	glUniform1f(p_program_data.m_scale_factor_uniform, m_scale_factor);
	if (p_program_data.m_border_and_flatness_uniform != -1)
		glUniform3f(p_program_data.m_border_and_flatness_uniform, m_border_width * 0.01f / m_scale_factor, m_border_variance, m_flatness);
	if (p_program_data.m_texlen_uniform != -1)
		glUniform1f(p_program_data.m_texlen_uniform, float(m_atlas_size));
	if (p_program_data.m_lighting_params_uniform != -1)
		glUniform4f(p_program_data.m_lighting_params_uniform, m_ao_strength, m_direct_lighting_strength, m_shininess, m_glossiness);

	// Render according to the specified pass
	// (Cylinder geometry data is placed right behind the sphere one)
	switch (p_pass)
	{
		case pass_spheres:   glDrawElements(GL_TRIANGLES, m_num_atoms * 6, GL_UNSIGNED_INT, 0); break;
		case pass_cylinders: glDrawElements(GL_TRIANGLES, m_num_bonds * 6, GL_UNSIGNED_INT, reinterpret_cast < GLvoid const * > (sizeof(std::uint32_t) * m_num_atoms * 6)); break;
	}

	// Unbind the GLSL program
	p_program_data.m_program.unbind();
}


void molecule_renderer::calc_ao()
{
	/*
	This function shadows the molecule from a number of direction (the exact number is defined in the num_directions constant).
	For each direction, a rotation matrix is computed using the lookat transform, and a shadowmap is created, which is then
	used to shadow the molecule. The shadowing is accumulated in the ao atlas texture, resulting in the final ambient occlusion data.
	*/

	unsigned int const depth_sidelength = m_ao_atlas->get_width();

	// For shadowmap generation, use a square orthogonal projection.
	matrix texture_proj;
	cml::matrix_orthographic_LH(texture_proj, -1.0f, +1.0f, -1.0f, +1.0f, 1.0f, 601.0f, cml::z_clip_neg_one);
	glViewport(0, 0, depth_sidelength, depth_sidelength);

	// Create the depth texture that will be used to hold the shadowmap
	texture depth_texture(depth_sidelength, depth_sidelength, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE);

	// FBO to be used for shadowmap and AO atlas render-to-texture.
	fbo fbo_;

	fbo_.bind();
	attach_to(fbo_, GL_DEPTH_ATTACHMENT, depth_texture);
	attach_to(fbo_, GL_COLOR_ATTACHMENT0, *m_ao_atlas);

	glClearColor(1,1,1,1);
	glClear(GL_COLOR_BUFFER_BIT);

	m_vao.bind();


	for (std::size_t dir_idx = 0; dir_idx < num_directions; ++dir_idx)
	{
		// Calculate the rotation using the lookat transform

		// First, an orthogonal basis needs to be found
		cml::vector3f const &dir1 = directions[dir_idx];
		cml::vector3f dir2 = cml::cross(dir1, cml::vector3f(1, 0, 0));
		if (cml::length_squared(dir2) < 0.1f)
			dir2 = cml::cross(dir1, cml::vector3f(0, 1, 0));
		dir2.normalize();
		cml::vector3f dir3 = cml::normalize(cml::cross(dir1, dir2));

		matrix rot;
		// With the basis defined by dir1 and dir3, use the lookat transform
		cml::matrix_look_at_RH(rot, dir1, cml::vector3f(0, 0, 0), dir3);


		// draw to shadowmap; disable all color writes, leave only depth writes on

		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
		glEnable(GL_DEPTH_TEST);
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glClear(GL_DEPTH_BUFFER_BIT);
		render_internal(m_shaders.m_sphere_draw_to_shadowmap, rot, texture_proj, pass_spheres);
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);


		// shadow the molecule and draw the result to the ao atlas; reenable color writes,
		// enable subtractive blending for accumulation, and disable depth testing and writing

		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
		depth_texture.bind(0);
		glDisable(GL_DEPTH_TEST);
		render_internal(m_shaders.m_sphere_draw_to_ao_atlas, rot, texture_proj, pass_spheres);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		depth_texture.unbind(0);		
	}

	// Cleanup
	m_vao.unbind();
	fbo_.unbind();
	glViewport(m_left_viewport_offset, m_top_viewport_offset, m_viewport_width, m_viewport_height);
}


void molecule_renderer::calc_quads()
{
	/*
	Calculates impostor geometry, which is made of quads.
	For each quad, 4 vertices and 6 indices are calculated.
	*/


	colors colors_;
	radii radii_;

	std::size_t total_num_quads, total_num_patches;
	if (rendering_mode_uses_cylinders(m_rendering_mode))
	{
		total_num_quads = m_num_atoms + m_num_bonds;
		total_num_patches = m_num_atoms + (m_num_bonds + 1) / 2;
	}
	else
	{
		total_num_quads = m_num_atoms;
		total_num_patches = m_num_atoms;
	}

	// The atlas is divided into patches, one patch for each atom. Bonds are paired, and each bond pair shares one patch
	// (= one half patch for each bond).
	m_ao_patch_sidelength = std::size_t(float(m_atlas_size) / std::ceil(std::sqrt(float(total_num_patches))));
	// The patch stride is the amount of patches each horizontal texture line contains
	std::size_t patch_stride = m_atlas_size / m_ao_patch_sidelength;

	cml::vector3f mincoords, maxcoords;

	// iterate over each atom to move the molecule to the origin
	{
		// first get the min/max coords
		for (auto atom_iter = m_data.m_atoms.begin(); atom_iter != m_data.m_atoms.end(); ++atom_iter)
		{
			molecule_data::atom const &cur_atom = atom_iter->second;

			if (atom_iter == m_data.m_atoms.begin())
			{
				mincoords = cur_atom.m_position;
				maxcoords = cur_atom.m_position;
			}
			else
			{
				for (int i = 0; i < 3; ++i)
				{
					mincoords[i] = std::min(mincoords[i], cur_atom.m_position[i]);
					maxcoords[i] = std::max(maxcoords[i], cur_atom.m_position[i]);
				}
			}
		}
	
		// Calculate the center of the molecule rendering
		cml::vector3f center = (mincoords + maxcoords) / 2.0f;

		// now adjust the atom positions, moving the center to (0,0,0) by subtracting it from the atom positions
		for (auto atom_iter = m_data.m_atoms.begin(); atom_iter != m_data.m_atoms.end(); ++atom_iter)
		{
			molecule_data::atom &cur_atom = atom_iter->second;
			cur_atom.m_position -= center;
		}
	}

	static cml::vector2f const st_sides[4] =
	{
		cml::vector2f(-1, -1),
		cml::vector2f( 1, -1),
		cml::vector2f(-1,  1),
		cml::vector2f( 1,  1)
	};

	static std::size_t const uv_offsets[4][2] =
	{
		{ 0, 0 },
		{ 1, 0 },
		{ 0, 1 },
		{ 1, 1 }
	};

	m_vao.bind();
	m_vertices.bind();
	m_indices.bind();

	// vertices
	{
		// * 4, because each atom & bond quad uses 4 vertices
		m_vertices.resize(sizeof(vertex) * (total_num_quads) * 4);
		buffer_object_mapping mapping(m_vertices, GL_MAP_WRITE_BIT);
		vertex *vtx = reinterpret_cast < vertex* > (mapping.get_mapped_pointer());

		// atom vertices
		std::size_t atom_cnt = 0;
		for (auto atom_iter = m_data.m_atoms.begin(); atom_iter != m_data.m_atoms.end(); ++atom_iter, ++atom_cnt)
		{
			molecule_data::atom const &cur_atom = atom_iter->second;

			cml::vector3f color;

			switch (m_atom_color_mode)
			{
				case atom_color_mode_per_element:
					color = colors_.get_color(cur_atom.m_name); break;
				case atom_color_mode_per_chain:
					color = colors_.get_chain_color(cur_atom.m_chain_id); break;
				default:
					assert(false);
			}

			float radius = radii_.get_radius(cur_atom.m_name) * m_radius_scale;

			std::size_t y = atom_cnt / patch_stride;
			float v[2] =
			{
				float((y + 0) * m_ao_patch_sidelength + 0.5f) / m_atlas_size,
				float((y + 1) * m_ao_patch_sidelength + 0.5f) / m_atlas_size
			};

			for (int corner_index = 0; corner_index < 4; ++corner_index)
			{
				std::size_t x = atom_cnt % patch_stride;
				float u[2] =
				{
					float((x + 0) * m_ao_patch_sidelength + 0.5f) / m_atlas_size,
					float((x + 1) * m_ao_patch_sidelength + 0.5f) / m_atlas_size
				};

				std::size_t offset = atom_cnt * 4 + corner_index;
				vtx[offset].m_position = cur_atom.m_position;
				vtx[offset].m_st = st_sides[corner_index];
				vtx[offset].m_uv = cml::vector2f(u[uv_offsets[corner_index][0]], v[uv_offsets[corner_index][1]]);
				vtx[offset].m_uvquad = cml::vector4f(u[0], u[1], v[0], v[1]);
				if (rendering_mode_uses_cylinders(m_rendering_mode))
					vtx[offset].m_color_radius = cml::vector4f(color, radius * 0.3f); // TODO: remove the *0.3f once cylinders are done
				else
					vtx[offset].m_color_radius = cml::vector4f(color, radius * 1.0f);
			}
		}

		// bond vertices
		if (rendering_mode_uses_cylinders(m_rendering_mode))
		{
			atom_cnt = m_data.m_atoms.size();
			std::size_t bond_cnt = 0;
			for (auto bond_iter = m_data.m_bonds.begin(); bond_iter != m_data.m_bonds.end(); ++bond_iter, ++bond_cnt)
			{
				molecule_data::bond const &cur_bond = *bond_iter;
				molecule_data::atoms_type::const_iterator atom_iters[2];
				atom_iters[0] = m_data.m_atoms.find(cur_bond.m_serial_numbers[0]);
				atom_iters[1] = m_data.m_atoms.find(cur_bond.m_serial_numbers[1]);
				if ((atom_iters[0] == m_data.m_atoms.end()) || (atom_iters[1] == m_data.m_atoms.end()))
					continue;
				molecule_data::atom const *bond_atoms[2] = { &(atom_iters[0]->second), &(atom_iters[1]->second) };

				cml::vector3f dir = bond_atoms[1]->m_position - bond_atoms[0]->m_position;

				std::size_t y = (atom_cnt + bond_cnt / 2) / patch_stride;
				float v[2] =
				{
					float((y + 0) * m_ao_patch_sidelength + 0.5f) / m_atlas_size,
					float((y + 1) * m_ao_patch_sidelength + 0.5f) / m_atlas_size
				};

				for (int corner_index = 0; corner_index < 4; ++corner_index)
				{
					std::size_t x = (atom_cnt * 2 + bond_cnt) % (patch_stride * 2);
					float u[2] =
					{
						float((x + 0) * 0.5f * m_ao_patch_sidelength + 0.5f) / m_atlas_size,
						float((x + 1) * 0.5f * m_ao_patch_sidelength + 0.5f) / m_atlas_size
					};

					std::size_t offset = (atom_cnt + bond_cnt) * 4 + corner_index;

					assert(offset < (total_num_quads * 4));

					cml::vector3f const &color = colors_.get_color(bond_atoms[corner_index >> 1]->m_name);
					vtx[offset].m_position = bond_atoms[corner_index >> 1]->m_position;
					vtx[offset].m_st = st_sides[corner_index];
					vtx[offset].m_uv = cml::vector2f(u[uv_offsets[corner_index][0]], v[uv_offsets[corner_index][1]]);
					vtx[offset].m_uvquad = cml::vector4f(u[0], u[1], v[0], v[1]);
					vtx[offset].m_color_radius = cml::vector4f(color, (0.16f + 0.32f * 0.85f - 0.16f) * m_radius_scale); // TODO: figure out proper radius for cylinders & spheres
					vtx[offset].m_direction = dir;
				}
			}
		}
	}

	// indices
	{
		// * 6, because each atom & bond quad uses 6 indices
		m_indices.resize(sizeof(std::uint32_t) * (total_num_quads) * 6);
		buffer_object_mapping mapping(m_indices, GL_MAP_WRITE_BIT);
		std::uint32_t *idx = reinterpret_cast < std::uint32_t* > (mapping.get_mapped_pointer());

		// atom indices
		for (std::size_t atom_cnt = 0; atom_cnt != m_data.m_atoms.size(); ++atom_cnt)
		{
			std::size_t idxdata_ofs = atom_cnt * 6;
			std::size_t idx_vtxofs = atom_cnt * 4;

			idx[idxdata_ofs + 0] = idx_vtxofs + 0;
			idx[idxdata_ofs + 1] = idx_vtxofs + 1;
			idx[idxdata_ofs + 2] = idx_vtxofs + 2;
			idx[idxdata_ofs + 3] = idx_vtxofs + 2;
			idx[idxdata_ofs + 4] = idx_vtxofs + 1;
			idx[idxdata_ofs + 5] = idx_vtxofs + 3;
		}

		// bond indices
		if (rendering_mode_uses_cylinders(m_rendering_mode))
		{
			for (std::size_t bond_cnt = 0; bond_cnt != m_data.m_bonds.size(); ++bond_cnt)
			{
				std::size_t idxdata_ofs = (bond_cnt + m_data.m_atoms.size()) * 6;
				std::size_t idx_vtxofs = m_data.m_atoms.size() * 4 + bond_cnt * 4;

				idx[idxdata_ofs + 0] = idx_vtxofs + 0;
				idx[idxdata_ofs + 1] = idx_vtxofs + 1;
				idx[idxdata_ofs + 2] = idx_vtxofs + 2;
				idx[idxdata_ofs + 3] = idx_vtxofs + 2;
				idx[idxdata_ofs + 4] = idx_vtxofs + 1;
				idx[idxdata_ofs + 5] = idx_vtxofs + 3;
			}
		}
	}

	m_vao.unbind();

	float radius = cml::length(maxcoords - mincoords) / 2.0f;
	m_scale_factor = 1.0f / radius;

	m_base_translation = cml::vector3f(0, 0, cml::length(maxcoords - mincoords) * 0.5f + 3.0f);
}


}

