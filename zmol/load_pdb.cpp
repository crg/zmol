/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include <string>
#include <sstream>
#include <utility>
#include "load_pdb.hpp"


namespace zmol
{


namespace
{


template < typename T >
inline bool from_string(std::string const &p_str, T &p_result)
{
	std::stringstream sstr;
	return ((sstr << p_str) && (sstr >> p_result));
}


}


molecule_data load_pdb(std::istream &p_in)
{
	molecule_data data;

	while (true)
	{
		std::string line;
		std::getline(p_in, line);
		if (!p_in)
			break;

		std::string type = line.substr(0, 6);
		if ((type == "ATOM  ") || (type == "HETATM"))
		{
			std::size_t serial_nr;
			float x, y, z;
			if (!(
				from_string(line.substr(6, 5), serial_nr) &&
				from_string(line.substr(30, 8), x) &&
				from_string(line.substr(38, 8), y) &&
				from_string(line.substr(46, 8), z)
			))
				continue;

			molecule_data::atom new_atom;
			new_atom.m_name = line.substr(12, 4);
			new_atom.m_position = cml::vector3f(x, y, z);
			if (line[21] != 0)
				new_atom.m_chain_id = line[21];
			data.m_atoms[serial_nr] = new_atom;
		}
		else if (type == "CONECT")
		{
			molecule_data::bond new_bond;
			if (!from_string(line.substr(6, 5), new_bond.m_serial_numbers[0]))
				continue;

			for (std::size_t i = 0; i < 4; ++i)
			{
				if ((12 + i * 5) >= line.length())
					break;
				if (from_string(line.substr(11 + i * 5, 5), new_bond.m_serial_numbers[1]))
					data.m_bonds.push_back(new_bond);
			}
		}
	}

	return data;
}


}

