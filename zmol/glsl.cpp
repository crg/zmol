/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include <assert.h>
#include <vector>
#include <algorithm>
#include "glsl.hpp"


namespace zmol
{


shader::shader(GLenum const p_type):
	m_name(0)
{
	m_name = glCreateShader(p_type);
}


shader::~shader()
{
	if (m_name != 0)
		glDeleteShader(m_name);
}


bool shader::compile_and_store(std::string const &p_code)
{
	char const *code_cstr = p_code.c_str();
	GLint length = p_code.length();
	glShaderSource(m_name, 1, &code_cstr, &length);
	glCompileShader(m_name);

	GLint status;
	glGetShaderiv(m_name, GL_COMPILE_STATUS, &status);
	return status == GL_TRUE;
}


std::string shader::get_info_log() const
{
	GLsizei max_length, length;
	glGetShaderiv(m_name, GL_INFO_LOG_LENGTH, &max_length);

	std::vector < char > temp_log;
	temp_log.resize(max_length + 1); // +1 since OpenGL fills in the null terminator
	glGetShaderInfoLog(m_name, max_length, &length, &(temp_log[0]));
	temp_log.resize(length); // NOTE: the null terminator is IGNORED here

	std::string log;
	log.resize(length);
	std::copy(temp_log.begin(), temp_log.end(), log.begin());

	return log;
}





program::program():
	m_name(0)
{
	m_name = glCreateProgram();
}


program::~program()
{
	if (m_name != 0)
	{
		while (!m_attached_shaders.empty())
		{
			shader::sharedptr shader = *(m_attached_shaders.begin());
			assert(shader);
			m_attached_shaders.erase(m_attached_shaders.begin());

			if ((m_name != 0) && (shader->get_name() != 0))
				glDetachShader(m_name, shader->get_name());
		}

		glDeleteProgram(m_name);
	}
}


void program::attach_shader(shader::sharedptr const &p_shader)
{
	if (m_attached_shaders.find(p_shader) != m_attached_shaders.end())
		return; // shader already attached

	m_attached_shaders.insert(p_shader);
	glAttachShader(m_name, p_shader->get_name());
}


void program::detach_shader(shader::sharedptr const &p_shader)
{
	glsl_shader_set_type::iterator shader_iter = m_attached_shaders.find(p_shader);

	if (shader_iter == m_attached_shaders.end())
		return; // shader not attached

	m_attached_shaders.erase(shader_iter);
	glDetachShader(m_name, p_shader->get_name());
}


bool program::link()
{
	glLinkProgram(m_name);

	GLint status;
	glGetProgramiv(m_name, GL_LINK_STATUS, &status);
	return status == GL_TRUE;
}


void program::bind()
{
	glUseProgram(m_name);
}


void program::unbind()
{
	glUseProgram(0);
}


GLint program::get_attribute_location(std::string const &p_attribute_name) const
{
	locations_type::const_iterator iter = m_attribute_locations.find(p_attribute_name);
	if (iter != m_attribute_locations.end())
		return iter->second;

	GLint location = glGetAttribLocation(m_name, reinterpret_cast < GLchar const * > (p_attribute_name.c_str()));
	m_attribute_locations.insert(locations_type::value_type(p_attribute_name, location));
	return location;
}


GLint program::get_uniform_location(std::string const &p_uniform_name) const
{
	locations_type::const_iterator iter = m_uniform_locations.find(p_uniform_name);
	if (iter != m_uniform_locations.end())
		return iter->second;

	GLint location = glGetUniformLocation(m_name, reinterpret_cast < GLchar const * > (p_uniform_name.c_str()));
	m_uniform_locations.insert(locations_type::value_type(p_uniform_name, location));
	return location;
}


GLuint program::get_uniform_block_index(std::string const &p_block_name) const
{
	indices_type::const_iterator iter = m_uniform_block_indices.find(p_block_name);
	if (iter != m_uniform_block_indices.end())
		return iter->second;

	GLuint index = glGetUniformBlockIndex(m_name, reinterpret_cast < GLchar const * > (p_block_name.c_str()));
	m_uniform_block_indices.insert(indices_type::value_type(p_block_name, index));
	return index;
}


std::string program::get_info_log() const
{
	GLsizei max_length, length;
	glGetProgramiv(m_name, GL_INFO_LOG_LENGTH, &max_length);

	std::vector < char > temp_log;
	temp_log.resize(max_length + 1); // +1 since OpenGL fills in the null terminator
	glGetProgramInfoLog(m_name, max_length, &length, &(temp_log[0]));
	temp_log.resize(length); // NOTE: the null terminator is IGNORED here

	std::string log;
	log.resize(length);
	std::copy(temp_log.begin(), temp_log.end(), log.begin());

	return log;
}





namespace detail
{


build_program_impl::build_program_impl(program &p_prog, errormsg_callback const &p_errormsg_callback):
	m_prog(p_prog),
	m_errormsg_callback(p_errormsg_callback)
{
}


build_program_impl& build_program_impl::operator()(GLenum const p_type, std::string const &p_identifier, std::string const &p_code)
{
	shader::sharedptr shader_ = shader::sharedptr(new shader(p_type));
	if (!shader_->compile_and_store(p_code))
	{
		if (m_errormsg_callback)
		{
			std::string msg("could not compile \"" + p_identifier + "\":" + shader_->get_info_log());
			m_errormsg_callback(msg);
		}
	}

	m_prog.attach_shader(shader_);

	return *this;
}


build_program_impl& build_program_impl::operator()(shader::sharedptr const &p_shader)
{
	if (p_shader)
		m_prog.attach_shader(p_shader);
	return *this;
}


void build_program_impl::link()
{
	if (!m_prog.link())
	{
		if (m_errormsg_callback)
		{
			std::string msg("could not link program:" + m_prog.get_info_log());
			m_errormsg_callback(msg);
		}
	}
}


} // namespace detail end


detail::build_program_impl build_program(program &p_prog, detail::errormsg_callback const &p_errormsg_callback)
{
	return detail::build_program_impl(p_prog, p_errormsg_callback);
}


}

