/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include "fbo.hpp"


namespace zmol
{


fbo::fbo(GLenum const p_target):
	m_name(0),
	m_target(p_target)
{
	glGenFramebuffers(1, &m_name);
}


fbo::~fbo()
{
	if (m_name != 0)
		glDeleteFramebuffers(1, &m_name);
}


void fbo::bind()
{
	glBindFramebuffer(m_target, m_name);
}


void fbo::unbind()
{
	glBindFramebuffer(m_target, 0);
}




renderbuffer::renderbuffer(GLenum const p_format, GLsizei const p_width, GLsizei const p_height, GLsizei const p_num_antialiasing_samples)
{
	glGenRenderbuffers(1, &m_name);
	bind();
	if (p_num_antialiasing_samples == 0)
		glRenderbufferStorage(GL_RENDERBUFFER, p_format, p_width, p_height);
	else
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, p_num_antialiasing_samples, p_format, p_width, p_height);
	unbind();
}


renderbuffer::~renderbuffer()
{
	glDeleteRenderbuffers(1, &m_name);
}


void renderbuffer::bind()
{
	glBindRenderbuffer(GL_RENDERBUFFER, m_name);
}


void renderbuffer::unbind()
{
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}




void attach_to(fbo &p_fbo, GLenum const p_attachment, renderbuffer const &p_renderbuf)
{
	glFramebufferRenderbuffer(p_fbo.get_target(), p_attachment, GL_RENDERBUFFER, p_renderbuf.get_name());
}


void attach_to(fbo &p_fbo, GLenum const p_attachment, texture const &p_texture)
{
	glFramebufferTexture2D(p_fbo.get_target(), p_attachment, GL_TEXTURE_2D, p_texture.get_name(), 0);
}


void detach_from(fbo &p_fbo, GLenum const p_attachment, renderbuffer const &)
{
	glFramebufferRenderbuffer(p_fbo.get_target(), p_attachment, GL_RENDERBUFFER, 0);
}


void detach_from(fbo &p_fbo, GLenum const p_attachment, texture const &)
{
	glFramebufferTexture2D(p_fbo.get_target(), p_attachment, GL_TEXTURE_2D, 0, 0);
}


void set_draw_buffer(GLenum const p_buffer)
{
	glDrawBuffer(p_buffer);
}


void set_draw_buffers(GLsizei const p_num_buffers, GLenum const * p_buffers)
{
	glDrawBuffers(p_num_buffers, p_buffers);
}


}

