/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#include "directions.hpp"


namespace zmol
{


cml::vector3f const directions[num_directions] =
{
	cml::vector3f(0.000000f, -1.000000f, 0.000000f),
	cml::vector3f(0.723600f, -0.447215f, 0.525720f),
	cml::vector3f(-0.276385f, -0.447215f, 0.850640f),
	cml::vector3f(-0.894425f, -0.447215f, 0.000000f),
	cml::vector3f(-0.276385f, -0.447215f, -0.850640f),
	cml::vector3f(0.723600f, -0.447215f, -0.525720f),
	cml::vector3f(0.276385f, 0.447215f, 0.850640f),
	cml::vector3f(-0.723600f, 0.447215f, 0.525720f),
	cml::vector3f(-0.723600f, 0.447215f, -0.525720f),
	cml::vector3f(0.276385f, 0.447215f, -0.850640f),
	cml::vector3f(0.894425f, 0.447215f, 0.000000f),
	cml::vector3f(0.000000f, 1.000000f, 0.000000f),
	cml::vector3f(-0.162456f, -0.850654f, 0.499995f),
	cml::vector3f(0.425323f, -0.850654f, 0.309011f),
	cml::vector3f(0.262869f, -0.525738f, 0.809012f),
	cml::vector3f(0.425323f, -0.850654f, -0.309011f),
	cml::vector3f(0.850648f, -0.525736f, 0.000000f),
	cml::vector3f(-0.525730f, -0.850652f, 0.000000f),
	cml::vector3f(-0.688189f, -0.525736f, 0.499997f),
	cml::vector3f(-0.162456f, -0.850654f, -0.499995f),
	cml::vector3f(-0.688189f, -0.525736f, -0.499997f),
	cml::vector3f(0.262869f, -0.525738f, -0.809012f),
	cml::vector3f(0.951058f, 0.000000f, -0.309013f),
	cml::vector3f(0.951058f, 0.000000f, 0.309013f),
	cml::vector3f(0.587786f, 0.000000f, 0.809017f),
	cml::vector3f(0.000000f, 0.000000f, 1.000000f),
	cml::vector3f(-0.587786f, 0.000000f, 0.809017f),
	cml::vector3f(-0.951058f, 0.000000f, 0.309013f),
	cml::vector3f(-0.951058f, 0.000000f, -0.309013f),
	cml::vector3f(-0.587786f, 0.000000f, -0.809017f),
	cml::vector3f(0.000000f, 0.000000f, -1.000000f),
	cml::vector3f(0.587786f, 0.000000f, -0.809017f),
	cml::vector3f(0.688189f, 0.525736f, 0.499997f),
	cml::vector3f(-0.262869f, 0.525738f, 0.809012f),
	cml::vector3f(-0.850648f, 0.525736f, 0.000000f),
	cml::vector3f(-0.262869f, 0.525738f, -0.809012f),
	cml::vector3f(0.688189f, 0.525736f, -0.499997f),
	cml::vector3f(0.525730f, 0.850652f, 0.000000f),
	cml::vector3f(0.162456f, 0.850654f, 0.499995f),
	cml::vector3f(-0.425323f, 0.850654f, 0.309011f),
	cml::vector3f(-0.425323f, 0.850654f, -0.309011f),
	cml::vector3f(0.162456f, 0.850654f, -0.499995f),
	cml::vector3f(-0.084442f, -0.961939f, 0.259889f),
	cml::vector3f(-0.228103f, -0.674615f, 0.702042f),
	cml::vector3f(0.221076f, -0.961939f, 0.160619f),
	cml::vector3f(0.597194f, -0.674615f, 0.433882f),
	cml::vector3f(-0.007026f, -0.505728f, 0.862665f),
	cml::vector3f(0.512753f, -0.505727f, 0.693775f),
	cml::vector3f(0.221076f, -0.961939f, -0.160619f),
	cml::vector3f(0.597194f, -0.674615f, -0.433882f),
	cml::vector3f(0.818272f, -0.505726f, 0.273262f),
	cml::vector3f(0.818272f, -0.505726f, -0.273262f),
	cml::vector3f(-0.273266f, -0.961939f, 0.000000f),
	cml::vector3f(-0.738174f, -0.674610f, 0.000000f),
	cml::vector3f(-0.501373f, -0.505727f, 0.702043f),
	cml::vector3f(-0.822618f, -0.505724f, 0.259890f),
	cml::vector3f(-0.084442f, -0.961939f, -0.259889f),
	cml::vector3f(-0.228103f, -0.674615f, -0.702042f),
	cml::vector3f(-0.501373f, -0.505727f, -0.702043f),
	cml::vector3f(-0.822618f, -0.505724f, -0.259890f),
	cml::vector3f(0.512753f, -0.505727f, -0.693775f),
	cml::vector3f(-0.007026f, -0.505728f, -0.862665f),
	cml::vector3f(0.959253f, 0.232455f, -0.160620f),
	cml::vector3f(0.870465f, -0.232456f, -0.433883f),
	cml::vector3f(0.959253f, 0.232455f, 0.160620f),
	cml::vector3f(0.870465f, -0.232456f, 0.433883f),
	cml::vector3f(0.681641f, -0.232457f, 0.693779f),
	cml::vector3f(0.449185f, 0.232457f, 0.862668f),
	cml::vector3f(-0.143661f, -0.232456f, 0.961938f),
	cml::vector3f(0.143661f, 0.232456f, 0.961938f),
	cml::vector3f(-0.449185f, -0.232457f, 0.862668f),
	cml::vector3f(-0.681641f, 0.232457f, 0.693779f),
	cml::vector3f(-0.870465f, 0.232456f, 0.433883f),
	cml::vector3f(-0.959253f, -0.232455f, 0.160620f),
	cml::vector3f(-0.870465f, 0.232456f, -0.433883f),
	cml::vector3f(-0.959253f, -0.232455f, -0.160620f),
	cml::vector3f(-0.681641f, 0.232457f, -0.693779f),
	cml::vector3f(-0.449185f, -0.232457f, -0.862668f),
	cml::vector3f(0.143661f, 0.232456f, -0.961938f),
	cml::vector3f(-0.143661f, -0.232456f, -0.961938f),
	cml::vector3f(0.449185f, 0.232457f, -0.862668f),
	cml::vector3f(0.681641f, -0.232457f, -0.693779f),
	cml::vector3f(0.822618f, 0.505724f, 0.259890f),
	cml::vector3f(0.501373f, 0.505727f, 0.702043f),
	cml::vector3f(-0.512753f, 0.505727f, 0.693775f),
	cml::vector3f(0.007026f, 0.505728f, 0.862665f),
	cml::vector3f(-0.818272f, 0.505726f, -0.273262f),
	cml::vector3f(-0.818272f, 0.505726f, 0.273262f),
	cml::vector3f(0.007026f, 0.505728f, -0.862665f),
	cml::vector3f(-0.512753f, 0.505727f, -0.693775f),
	cml::vector3f(0.822618f, 0.505724f, -0.259890f),
	cml::vector3f(0.501373f, 0.505727f, -0.702043f),
	cml::vector3f(0.273266f, 0.961939f, 0.000000f),
	cml::vector3f(0.738174f, 0.674610f, 0.000000f),
	cml::vector3f(0.084442f, 0.961939f, 0.259889f),
	cml::vector3f(0.228103f, 0.674615f, 0.702042f),
	cml::vector3f(-0.221076f, 0.961939f, 0.160619f),
	cml::vector3f(-0.597194f, 0.674615f, 0.433882f),
	cml::vector3f(-0.221076f, 0.961939f, -0.160619f),
	cml::vector3f(-0.597194f, 0.674615f, -0.433882f),
	cml::vector3f(0.084442f, 0.961939f, -0.259889f),
	cml::vector3f(0.228103f, 0.674615f, -0.702042f),
	cml::vector3f(0.052790f, -0.723612f, 0.688185f),
	cml::vector3f(0.138197f, -0.894430f, 0.425320f),
	cml::vector3f(0.361804f, -0.723612f, 0.587779f),
	cml::vector3f(0.670817f, -0.723611f, 0.162457f),
	cml::vector3f(0.447210f, -0.894429f, 0.000000f),
	cml::vector3f(0.670817f, -0.723611f, -0.162457f),
	cml::vector3f(-0.638195f, -0.723609f, 0.262864f),
	cml::vector3f(-0.361800f, -0.894429f, 0.262863f),
	cml::vector3f(-0.447211f, -0.723611f, 0.525727f),
	cml::vector3f(-0.447211f, -0.723611f, -0.525727f),
	cml::vector3f(-0.361800f, -0.894429f, -0.262863f),
	cml::vector3f(-0.638195f, -0.723609f, -0.262864f),
	cml::vector3f(0.361804f, -0.723612f, -0.587779f),
	cml::vector3f(0.138197f, -0.894430f, -0.425320f),
	cml::vector3f(0.052790f, -0.723612f, -0.688185f),
	cml::vector3f(0.947213f, -0.276396f, 0.162458f),
	cml::vector3f(0.947213f, -0.276396f, -0.162458f),
	cml::vector3f(1.000000f, 0.000000f, 0.000000f),
	cml::vector3f(0.138199f, -0.276397f, 0.951055f),
	cml::vector3f(0.447216f, -0.276397f, 0.850648f),
	cml::vector3f(0.309017f, 0.000000f, 0.951056f),
	cml::vector3f(-0.861804f, -0.276396f, 0.425322f),
	cml::vector3f(-0.670820f, -0.276396f, 0.688190f),
	cml::vector3f(-0.809018f, 0.000000f, 0.587783f),
	cml::vector3f(-0.670820f, -0.276396f, -0.688190f),
	cml::vector3f(-0.861804f, -0.276396f, -0.425322f),
	cml::vector3f(-0.809018f, 0.000000f, -0.587783f),
	cml::vector3f(0.447216f, -0.276397f, -0.850648f),
	cml::vector3f(0.138199f, -0.276397f, -0.951055f),
	cml::vector3f(0.309017f, 0.000000f, -0.951056f),
	cml::vector3f(0.670820f, 0.276396f, 0.688190f),
	cml::vector3f(0.809018f, 0.000000f, 0.587783f),
	cml::vector3f(0.861804f, 0.276396f, 0.425322f),
	cml::vector3f(-0.447216f, 0.276397f, 0.850648f),
	cml::vector3f(-0.309017f, 0.000000f, 0.951056f),
	cml::vector3f(-0.138199f, 0.276397f, 0.951055f),
	cml::vector3f(-0.947213f, 0.276396f, -0.162458f),
	cml::vector3f(-1.000000f, 0.000000f, 0.000000f),
	cml::vector3f(-0.947213f, 0.276396f, 0.162458f),
	cml::vector3f(-0.138199f, 0.276397f, -0.951055f),
	cml::vector3f(-0.309017f, 0.000000f, -0.951056f),
	cml::vector3f(-0.447216f, 0.276397f, -0.850648f),
	cml::vector3f(0.861804f, 0.276396f, -0.425322f),
	cml::vector3f(0.809018f, 0.000000f, -0.587783f),
	cml::vector3f(0.670820f, 0.276396f, -0.688190f),
	cml::vector3f(0.447211f, 0.723611f, 0.525727f),
	cml::vector3f(0.638195f, 0.723609f, 0.262864f),
	cml::vector3f(0.361800f, 0.894429f, 0.262863f),
	cml::vector3f(-0.361804f, 0.723612f, 0.587779f),
	cml::vector3f(-0.052790f, 0.723612f, 0.688185f),
	cml::vector3f(-0.138197f, 0.894430f, 0.425320f),
	cml::vector3f(-0.670817f, 0.723611f, -0.162457f),
	cml::vector3f(-0.670817f, 0.723611f, 0.162457f),
	cml::vector3f(-0.447210f, 0.894429f, 0.000000f),
	cml::vector3f(-0.052790f, 0.723612f, -0.688185f),
	cml::vector3f(-0.361804f, 0.723612f, -0.587779f),
	cml::vector3f(-0.138197f, 0.894430f, -0.425320f),
	cml::vector3f(0.638195f, 0.723609f, -0.262864f),
	cml::vector3f(0.447211f, 0.723611f, -0.525727f),
	cml::vector3f(0.361800f, 0.894429f, -0.262863f)
};


}

