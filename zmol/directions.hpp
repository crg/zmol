/****************************************************************************

Copyright (c) 2012 Carlos Rafael Giani  ( email: dv xxx AT pseudoterminal xxx DOT xxx org , remove the xxx )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

****************************************************************************/



#ifndef ZMOL_DIRECTIONS_HPP
#define ZMOL_DIRECTIONS_HPP

#include "cml/cml.h"


namespace zmol
{


// Number of hard-coded directions to use in the AO generation step.
enum { num_directions = 162 };
// Hard-coded directions to use in the AO generation step.
// These directions are distributed uniformly.
extern cml::vector3f const directions[num_directions];


}


#endif

