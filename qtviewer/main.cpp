#include "zmol/opengl3.hpp"
#include <QApplication>
#include <QGLFormat>
#include "gl_widget.hpp"
#include "main_window.hpp"
 
 
int main(int argc, char* argv[])
{
	QApplication a(argc, argv);

	main_window mw;
	mw.show();

	return a.exec();
}

