#include <iostream>
#include <fstream>
#include <cmath>
#include <utility>
#include "gl_widget.hpp"
#include "zmol/load_pdb.hpp"
#include <QCoreApplication>
#include <QMouseEvent>


namespace
{


std::string test_pdb_data =
"ATOM      1  H           1      -2.000   0.000   0.000\n"
"ATOM      2  H           1       2.000   0.000   0.000\n"
"ATOM      3  H           1       0.000  -2.000   0.000\n"
"ATOM      4  H           1       0.000   2.000   0.000\n"
"ATOM      5  H           1       0.000   0.000  -2.000\n"
"ATOM      6  H           1       0.000   0.000   2.000\n"
"ATOM      7 FE           1       0.000   0.000   0.000\n"
"CONECT    7    1\n"
"CONECT    7    2\n"
"CONECT    7    3\n"
;


}


gl_widget::gl_widget(QWidget *p_parent, QGLFormat const &p_format, std::string const &p_initial_file_to_load):
	QGLWidget(p_format, p_parent),
	m_dragging(false),
	m_initial_file_to_load(p_initial_file_to_load)
{
	m_rotation.identity();
	m_old_rot.identity();
	cml::quaternion_rotation_axis_angle(m_rotation, cml::vector3f(0.818272f, -0.505726f, 0.273262f), -3.1415926535f * 0.25f);
}


gl_widget::~gl_widget()
{
}


void gl_widget::load(
	std::istream &p_in,
	float const p_initial_border_width,
	float const p_initial_border_variance,
	float const p_initial_radius_scale,
	float const p_initial_ambient_occlusion,
	float const p_initial_direct_lighting,
	float const p_initial_shininess,
	float const p_initial_glossiness,
	float const p_initial_flatness,
	zmol::molecule_renderer::atom_color_modes const p_initial_atom_color_mode
)
{
	load_impl(
		p_in,
		p_initial_border_width,
		p_initial_border_variance,
		p_initial_radius_scale,
		p_initial_ambient_occlusion,
		p_initial_direct_lighting,
		p_initial_shininess,
		p_initial_glossiness,
		p_initial_flatness,
		p_initial_atom_color_mode
	);
	updateGL();
}


void gl_widget::load_impl(
	std::istream &p_in,
	float const p_initial_border_width,
	float const p_initial_border_variance,
	float const p_initial_radius_scale,
	float const p_initial_ambient_occlusion,
	float const p_initial_direct_lighting,
	float const p_initial_shininess,
	float const p_initial_glossiness,
	float const p_initial_flatness,
	zmol::molecule_renderer::atom_color_modes const p_initial_atom_color_mode
)
{
	try
	{
		zmol::molecule_data data = zmol::load_pdb(p_in);
		if (data.m_atoms.empty())
			return;

		m_molecule = molptr_type(new zmol::molecule_renderer(
			*m_shaders,
			std::move(data),
			0, 0, width(), height(),
			2048,
			p_initial_radius_scale,
			zmol::molecule_renderer::rendering_mode_space_filling,
			p_initial_atom_color_mode
		));
		m_molecule->set_border_width(p_initial_border_width);
		m_molecule->set_border_variance(p_initial_border_variance);
		m_molecule->set_ao_strength(p_initial_ambient_occlusion);
		m_molecule->set_direct_lighting_strength(p_initial_direct_lighting);
		m_molecule->set_shininess(p_initial_shininess);
		m_molecule->set_glossiness(p_initial_glossiness);
		m_molecule->set_flatness(p_initial_flatness);
	}
	catch (std::exception const &exc)
	{
		std::cerr << exc.what() << std::endl;
	}
}


zmol::molecule_renderer * gl_widget::get_molecule_renderer()
{
	if (m_molecule)
		return m_molecule.get();
	else
		return 0;
}


namespace
{


inline cml::vector3f mouseevent_to_3d(zmol::molecule_renderer const &p_molecule, QMouseEvent *event, QWidget *p_widget)
{
	float x = float(event->x()) / float(p_widget->width()) * 2.0f - 1.0f;
	float y = float(event->y()) / float(p_widget->height()) * 2.0f - 1.0f;
	cml::vector3f v(x, -y, -1.0f);
	zmol::molecule_renderer::matrix im;
	im.identity();
	return cml::unproject_point(im, p_molecule.get_projection(), im, v);
}


}


void gl_widget::mouseMoveEvent(QMouseEvent *event)
{
	if (!m_molecule || !m_dragging)
		return;

	m_end = mouseevent_to_3d(*m_molecule, event, this);

	cml::vector3f c(0, 0, 1.5f);
	cml::vector3f a = cml::normalize(m_start - c);
	cml::vector3f b = cml::normalize(m_end - c);

	cml::vector3f axis = cml::cross(a, b);
	float angle = std::acos(cml::dot(a, b));

	cml::quaternion_rotation_axis_angle(m_cur_rot, axis, angle);

	m_rotation = m_cur_rot * m_old_rot;
	m_rotation.normalize();

	updateGL();
}


void gl_widget::mousePressEvent(QMouseEvent *event)
{
	if (!m_molecule || m_dragging)
		return;

	m_old_rot = m_rotation;
	m_cur_rot.identity();
	m_start = mouseevent_to_3d(*m_molecule, event, this);
	m_dragging = true;
}


void gl_widget::mouseReleaseEvent(QMouseEvent *event)
{
	if (!m_molecule || !m_dragging)
		return;

	m_dragging = false;
}


void gl_widget::initializeGL()
{
	gl3wInit();
	glViewport(0, 0, width(), height());

	m_shaders = shadersptr_type(new zmol::molecule_shaders([](std::string const &p_errormsg) { std::cerr << p_errormsg << '\n'; } ));

	if (m_initial_file_to_load.empty())
	{
		std::istringstream in(test_pdb_data);
		load_impl(
			in,
			0.0f,
			1.0f,
			1.0f,
			1.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			zmol::molecule_renderer::atom_color_mode_per_element
		);
	}
	else
	{
		std::ifstream in(m_initial_file_to_load.c_str());
		if (!in.good())
			return;

		load_impl(
			in,
			0.0f,
			1.0f,
			1.0f,
			1.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			zmol::molecule_renderer::atom_color_mode_per_element
		);
	}
}


void gl_widget::paintGL()
{
	glClearColor(0.4f, 0.5f, 0.6f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (!m_molecule)
		return;

	m_molecule->render(m_rotation);
}


void gl_widget::resizeGL(int width, int height)
{
	glViewport(0, 0, width, height);
	if (m_molecule)
		m_molecule->set_viewport(0, 0, width, height);
}

