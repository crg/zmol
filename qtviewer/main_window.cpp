#include "zmol/opengl3.hpp"
#include "gl_widget.hpp"
#include <fstream>
#include <QCoreApplication>
#include <QGLFormat>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QDockWidget>
#include "main_window.hpp"
#include "ui_mainwindow.h"
#include "ui_sidebar.h"


namespace
{


struct preset
{
	float
		m_ambient_occlusion_strength,
		m_direct_lighting_strength,
		m_shininess,
		m_glossiness,
		m_flatness,
		m_border_width;
	char const *m_name;
};


preset presets[] = {
	{ 0.8f, 0.3f, 0.6f, 0.4f, 0.0f, 0.0f,  "Mixed" },
	{ 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,  "Ambient occlusion only" },
	{ 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  "Direct lighting only" },
	{ 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 3.0f,  "Poster" },

	{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,  0 }
};


}


main_window::main_window()
{
	// Specify an OpenGL 3.3 format using the Core profile.
	// That is, no old-school fixed pipeline functionality
	QGLFormat gl_format;
	gl_format.setVersion(3, 3);
	gl_format.setProfile(QGLFormat::CoreProfile); // Requires >=Qt-4.8.0
	gl_format.setSampleBuffers(true);

	Ui_main_window mainwindow_ui;
	mainwindow_ui.setupUi(this);

	QStringList args = QCoreApplication::arguments();
	std::string fname;
	if (args.size() > 1)
		fname = args[1].toStdString();

	m_glwidget = new gl_widget(mainwindow_ui.centralwidget, gl_format, fname);

	QHBoxLayout *layout = new QHBoxLayout;
	layout->addWidget(m_glwidget);
	mainwindow_ui.centralwidget->setLayout(layout);

	QDockWidget *sidebar_dock = new QDockWidget("Sidebar", this);
	QWidget *sidebar_widget = new QWidget(sidebar_dock);
	sidebar_dock->setWidget(sidebar_widget);
	sidebar_dock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);

	Ui_sidebar sidebar_ui;
	sidebar_ui.setupUi(sidebar_widget);
	sidebar_ui.atom_color_mode_input->addItem("by element", int(zmol::molecule_renderer::atom_color_mode_per_element));
	sidebar_ui.atom_color_mode_input->addItem("by chain", int(zmol::molecule_renderer::atom_color_mode_per_chain));

	connect(sidebar_ui.border_width_input, SIGNAL(valueChanged(double)), this, SLOT(border_width_changed(double)));
	connect(sidebar_ui.border_variance_input, SIGNAL(valueChanged(double)), this, SLOT(border_variance_changed(double)));
	connect(sidebar_ui.radius_scale_input, SIGNAL(valueChanged(double)), this, SLOT(radius_scale_changed(double)));
	connect(sidebar_ui.atom_color_mode_input, SIGNAL(activated(int)), this, SLOT(atom_color_mode_changed(int)));
	connect(sidebar_ui.ambient_occlusion_input, SIGNAL(valueChanged(double)), this, SLOT(ambient_occlusion_changed(double)));
	connect(sidebar_ui.direct_lighting_input, SIGNAL(valueChanged(double)), this, SLOT(direct_lighting_changed(double)));
	connect(sidebar_ui.shininess_input, SIGNAL(valueChanged(double)), this, SLOT(shininess_changed(double)));
	connect(sidebar_ui.glossiness_input, SIGNAL(valueChanged(double)), this, SLOT(glossiness_changed(double)));
	connect(sidebar_ui.flatness_input, SIGNAL(valueChanged(double)), this, SLOT(flatness_changed(double)));
	connect(sidebar_ui.presets_list, SIGNAL(itemSelectionChanged()), this, SLOT(preset_selected()));

	m_atom_color_mode_input = sidebar_ui.atom_color_mode_input;
	m_border_width_input = sidebar_ui.border_width_input;
	m_border_variance_input = sidebar_ui.border_variance_input;
	m_radius_scale_input = sidebar_ui.radius_scale_input;
	m_ambient_occlusion_input = sidebar_ui.ambient_occlusion_input;
	m_direct_lighting_input = sidebar_ui.direct_lighting_input;
	m_shininess_input = sidebar_ui.shininess_input;
	m_glossiness_input = sidebar_ui.glossiness_input;
	m_flatness_input = sidebar_ui.flatness_input;
	m_presets_list = sidebar_ui.presets_list;

	addDockWidget(Qt::RightDockWidgetArea, sidebar_dock);

	fill_presets();

	// the on_action_*_triggered() slots are automatically connected by mainwindow_ui.setupUi()
}


void main_window::on_action_open_triggered()
{
	QString filename = QFileDialog::getOpenFileName(this, "Open molecule data", QString(), "Protein Data Bank (*.pdb);;All files (*)");
	if (filename.isNull())
		return;

	std::ifstream in(filename.toStdString().c_str());
	if (!in.good())
		return;

	m_glwidget->load(
		in,
		m_border_width_input->value(),
		m_border_variance_input->value(),
		m_radius_scale_input->value(),
		m_ambient_occlusion_input->value(),
		m_direct_lighting_input->value(),
		m_shininess_input->value(),
		m_glossiness_input->value(),
		m_flatness_input->value(),
		zmol::molecule_renderer::atom_color_modes(m_atom_color_mode_input->itemData(m_atom_color_mode_input->currentIndex()).toInt())
	);
}


void main_window::on_action_quit_triggered()
{
	close();
}


void main_window::border_width_changed(double const p_new_border_width)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_border_width(p_new_border_width);
		m_glwidget->updateGL();
	}
}


void main_window::border_variance_changed(double const p_new_border_variance)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_border_variance(p_new_border_variance);
		m_glwidget->updateGL();
	}
}


void main_window::radius_scale_changed(double const p_new_radius_scale)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_radius_scale(p_new_radius_scale);
		m_glwidget->updateGL();
	}
}


void main_window::atom_color_mode_changed(int const p_new_atom_color_mode_index)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_atom_color_mode(zmol::molecule_renderer::atom_color_modes(m_atom_color_mode_input->itemData(p_new_atom_color_mode_index).toInt()));
		m_glwidget->updateGL();
	}
}


void main_window::ambient_occlusion_changed(double const p_new_ambient_occlusion)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_ao_strength(p_new_ambient_occlusion);
		m_glwidget->updateGL();
	}
}


void main_window::direct_lighting_changed(double const p_new_direct_lighting)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_direct_lighting_strength(p_new_direct_lighting);
		m_glwidget->updateGL();
	}
}


void main_window::shininess_changed(double const p_new_shininess)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_shininess(p_new_shininess);
		m_glwidget->updateGL();
	}
}


void main_window::glossiness_changed(double const p_new_glossiness)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_glossiness(p_new_glossiness);
		m_glwidget->updateGL();
	}
}


void main_window::flatness_changed(double const p_new_flatness)
{
	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();
	if (molrenderer != 0)
	{
		molrenderer->set_flatness(p_new_flatness);
		m_glwidget->updateGL();
	}
}


void main_window::preset_selected()
{
	QList < QListWidgetItem* > selection = m_presets_list->selectedItems();
	if (selection.empty())
		return;

	int preset_index = selection[0]->data(Qt::UserRole).toInt();
	preset const &preset_ = presets[preset_index];

	zmol::molecule_renderer *molrenderer = m_glwidget->get_molecule_renderer();

	m_ambient_occlusion_input->setValue(preset_.m_ambient_occlusion_strength);
	m_direct_lighting_input->setValue(preset_.m_direct_lighting_strength);
	m_shininess_input->setValue(preset_.m_shininess);
	m_glossiness_input->setValue(preset_.m_glossiness);
	m_flatness_input->setValue(preset_.m_flatness);
	m_border_width_input->setValue(preset_.m_border_width);
}


void main_window::fill_presets()
{
	for (preset const *p = presets; p->m_name != 0; ++p)
	{
		int index = int(p - presets);
		QListWidgetItem *item = new QListWidgetItem(m_presets_list);
		item->setText(p->m_name);
		item->setData(Qt::UserRole, index);
		m_presets_list->addItem(item);
	}
}



#ifndef WIN32
// This needs to be done for Unix builds, since the Waf build system is used in this case, and it requires including the generated .moc file
#include "main_window.moc"
#endif

