#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <QMainWindow>


class gl_widget;
class QComboBox;
class QDoubleSpinBox;
class QListWidget;


class main_window:
	public QMainWindow
{
	Q_OBJECT
public:
	main_window();

protected slots:
	void on_action_open_triggered();
	void on_action_quit_triggered();
	void border_width_changed(double const p_new_border_width);
	void border_variance_changed(double const p_new_border_variance);
	void radius_scale_changed(double const p_new_radius_scale);
	void atom_color_mode_changed(int const p_new_atom_color_mode_index);
	void ambient_occlusion_changed(double const p_new_ambient_occlusion);
	void direct_lighting_changed(double const p_new_direct_lighting);
	void shininess_changed(double const p_new_shininess);
	void glossiness_changed(double const p_new_glossiness);
	void flatness_changed(double const p_new_flatness);
	void preset_selected();

private:
	void fill_presets();

	gl_widget *m_glwidget;
	QDoubleSpinBox *m_border_width_input;
	QDoubleSpinBox *m_border_variance_input;
	QDoubleSpinBox *m_radius_scale_input;
	QDoubleSpinBox *m_ambient_occlusion_input;
	QDoubleSpinBox *m_direct_lighting_input;
	QDoubleSpinBox *m_shininess_input;
	QDoubleSpinBox *m_glossiness_input;
	QDoubleSpinBox *m_flatness_input;
	QComboBox *m_atom_color_mode_input;
	QListWidget *m_presets_list;
};


#endif

