#ifndef GL_WIDGET_HPP
#define GL_WIDGET_HPP

#include "zmol/opengl3.hpp"
#include <QGLWidget>
#include <QGLFormat>

#include <iostream>
#include <memory>
#include "zmol/molecule_renderer.hpp"


class gl_widget:
	public QGLWidget
{
public:
	explicit gl_widget(QWidget *p_parent, QGLFormat const &p_format, std::string const &p_initial_file_to_load);
	~gl_widget();

	void load(
		std::istream &p_in,
		float const p_initial_border_width,
		float const p_initial_border_variance,
		float const p_initial_radius_scale,
		float const p_initial_ambient_occlusion,
		float const p_initial_direct_lighting,
		float const p_initial_shininess,
		float const p_initial_glossiness,
		float const p_initial_flatness,
		zmol::molecule_renderer::atom_color_modes const p_initial_atom_color_mode
	);

	zmol::molecule_renderer * get_molecule_renderer();


protected:
	void load_impl(
		std::istream &p_in,
		float const p_initial_border_width,
		float const p_initial_border_variance,
		float const p_initial_radius_scale,
		float const p_initial_ambient_occlusion,
		float const p_initial_direct_lighting,
		float const p_initial_shininess,
		float const p_initial_glossiness,
		float const p_initial_flatness,
		zmol::molecule_renderer::atom_color_modes const p_initial_atom_color_mode
	);

	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent( QMouseEvent *event);

	virtual void initializeGL();
	virtual void paintGL();
	virtual void resizeGL(int width, int height);


	typedef std::unique_ptr < zmol::molecule_renderer > molptr_type;
	typedef std::unique_ptr < zmol::molecule_shaders > shadersptr_type;
	molptr_type m_molecule;
	shadersptr_type m_shaders;

	bool m_dragging;
	cml::quaternionf m_rotation, m_cur_rot, m_old_rot;
	cml::vector3f m_start, m_end;

	std::string const m_initial_file_to_load;
};


#endif

